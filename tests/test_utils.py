"""Test utility functions."""
import numpy as np
import pytest
import src.utils as utils

NUM_BINS = 10


@pytest.fixture(name="cont_obs")
def fixture_cont_obs():
    """Generate a an array of n m-dimensional continuous observations."""
    return np.random.uniform(low=-1.0, high=1.0, size=(9, 500))


def test_obs_bins(cont_obs):
    """Check that bins divide the input interval into equal ranges."""
    bins = utils.obs_bins(cont_obs, num_bins=NUM_BINS)
    assert np.allclose(bins, np.linspace(-1, 0.8, NUM_BINS), atol=2e-2)


def test_cont_to_disc(cont_obs):
    """Verify that the discrete observations lie within the bin ranges."""
    bins = utils.obs_bins(cont_obs, num_bins=NUM_BINS)
    disc_obs = utils.cont_to_disc(cont_obs, bins)
    assert np.max(disc_obs) == NUM_BINS - 1
    assert np.min(disc_obs) == 0
    assert disc_obs.shape == cont_obs.shape
