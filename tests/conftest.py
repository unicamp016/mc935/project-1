"""Common test config."""
import numpy as np
import pytest
from src.consts import Environment
from src.utils import unity_env

np.random.seed(42)


@pytest.fixture(scope="session")
def tennis_env():
    """Return the tennis environment."""
    with unity_env(Environment.TENNIS, no_graphics=True) as env:
        yield env
