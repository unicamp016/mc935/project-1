"""Test the main training script."""


def test_behavior_specs(tennis_env):
    """Verify the behavior specs for the tennis environment."""
    player1 = tennis_env.behavior_specs["Tennis?team=0"]
    player2 = tennis_env.behavior_specs["Tennis?team=1"]
    assert player1 == player2
    assert player1.action_spec.continuous_size == 3
    assert player1.action_spec.discrete_size == 0
    # The environment keeps track of the three previous observations.
    # Each one has only 9 dims. The last 9 are always new.
    assert player1.observation_shapes == [(27,)]
