# Directories
SRC_DIR	= src
TEST_DIR = tests
OUT_DIR = bin
REPORT_DIR = report

# Files
REPORT = report
PYTHON_FILES = $(wildcard $(SRC_DIR)/*.py $(TEST_DIR)/*.py)
UNITY_PROJECT_PATH = ml-agents/Project

.PHONY: test lint pylint pycodestyle pydocstyle build-report write-report \
	create_dirs clean

test:
	python -m pytest tests/ -vv

lint: pylint pycodestyle pydocstyle

pylint:
	@echo "\n------------------PYLINT------------------\n"
	pylint $(PYTHON_FILES) --disable=C --disable=R

pycodestyle:
	@echo "\n------------------PYCODESTYLE------------------\n"
	pycodestyle $(PYTHON_FILES) --max-line-length=90

pydocstyle:
	@echo "\n------------------PYDOCSTYLE------------------\n"
	pydocstyle $(PYTHON_FILES)

create_dirs:
	@for OUTPUT in $(shell cd $(REPORT_DIR); find . -type d); do \
	  mkdir -p $(OUT_DIR)/$(REPORT_DIR)/$$OUTPUT; \
	done

build-tennis-env: check-unity-path
	@echo "Building tennis environment..."
	@$(UNITY_EDITOR_PATH) -quit -batchmode -nographics \
		-projectPath $(UNITY_PROJECT_PATH) \
		-executeMethod Unity.MLAgents.SceneBuilder.BuildTennis
	@echo "Environment saved to $(UNITY_PROJECT_PATH)/Environments"

build-basic-env: check-unity-path
	@echo "Building basic environment..."
	@$(UNITY_EDITOR_PATH) -quit -batchmode -nographics \
		-projectPath $(UNITY_PROJECT_PATH) \
		-executeMethod Unity.MLAgents.SceneBuilder.BuildBasic
	@echo "Environment saved to $(UNITY_PROJECT_PATH)/Environments"

check-unity-path:
ifndef UNITY_EDITOR_PATH
	$(error UNITY_EDITOR_PATH is undefined)
endif

$(OUT_DIR):
	@mkdir -p $(OUT_DIR)

clean:
	@find . -name "*.pyc" -delete
	@rm -rf $(OUT_DIR)/*
