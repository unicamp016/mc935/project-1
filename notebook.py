# -*- coding: utf-8 -*-
# %%
# NOTE run this the first time:
# !wget -O requirements.txt https://pastebin.com/raw/jh8FTRXe
# !pip install -r requirements.txt

# %% ######################################################################### [markdown]
# # Vídeo: https://drive.google.com/file/d/13UctBeTEsqgSgUYU-O2R1Ro1tichLKfp/view?usp=sharing

# %% ######################################################################### [markdown]
# # Setup

# %%
%matplotlib inline
import sys

assert sys.version_info.major == 3
assert sys.version_info.minor >= 7
!python --version

# %%
try:
    import mlagents
    print("ml-agents already installed")
except ImportError:
    !pip install -q mlagents==0.23.0 mlagents_envs==0.23.0
    print("Installed ml-agents")

try:
    env.close()
except:
    pass

# %%
from mlagents_envs.registry import default_registry

env = default_registry["Basic"].make()
print("Opened environment")
env.reset()
env.close()
print("Closed environment")

# %%
from __future__ import annotations

import argparse
import enum
import os
from collections import OrderedDict, defaultdict
from contextlib import contextmanager
from enum import Enum
from itertools import accumulate
from os.path import abspath, dirname
from typing import (Any, DefaultDict, Dict, List, NamedTuple, Optional,
                    OrderedDict, Tuple, Union)

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from IPython.display import clear_output, display
from matplotlib.patches import Circle
from mlagents_envs.base_env import (ActionSpec, ActionTuple, BehaviorSpec,
                                    DecisionStep, DecisionSteps, TerminalSteps)
from mlagents_envs.environment import BaseEnv, UnityEnvironment
from mpl_toolkits.axes_grid1 import make_axes_locatable

# %%
# Basic environment
BASIC_ENV = "Basic"
BASIC_PLAYER = "Basic?team=0"
BASIC_OBS_NAMES = [
    # ml-agents/Project/Assets/ML-Agents/Examples/Basic/Scripts/BasicController.cs
    # NOTE the observation is a one-hot encoded vector with 1 for the current cube
    # position and 0 elsewhere (the small goal is at 7 and the large one is at 17)
    "cube_pos_1hot"  # 20 possible states (the agent/player starts at position 10)
]
BASIC_ACTIONS = OrderedDict({
    0: "stand",
    1: "left ",
    2: "right",
})

# %%
@contextmanager
def unity_env(env_name, **kwargs):
    """Context manager for a UnityEnvironment."""
    try:
        env_registry = default_registry[env_name]
    except KeyError as e:
        raise NotImplementedError(
            f"Requested environment '{env_name}' isn't available.") from e
    env = env_registry.make(**kwargs)
    env.reset()
    try:
        yield env
    finally:
        env.close()

# %%
class Method(enum.Enum):
    """Available reinforcement learning methods."""

    MONTE_CARLO = "Monte Carlo"
    SARSA_LAMBDA = "SARSA(λ)"
    Q_LEARNING = "Q-learning"

    MONTE_CARLO_LFA = "Monte Carlo (with Linear Function Approximator)"
    SARSA_LAMBDA_LFA = "SARSA(λ) (with Linear Function Approximator)"
    Q_LEARNING_LFA = "Q-learning (with Linear Function Approximator)"

# %%
class Config(NamedTuple):
    """Hyper-parameters.

    - Constant used to compute α and ε
    - Discount factor γ
    - Trace decay parameter λ (for SARSA(λ))
    - Number of episodes
    - Number of episodes between each visualization
    - Max number of steps per episode (unlimited if None)
    - Random seed
    """

    n0: int = 5000
    gamma: float = 0.95
    lambd: float = 0.60
    episodes: int = 300
    vis_freq: int = 1
    max_steps: Optional[int] = None

SEED: int = 123  # NOTE global random seed

MONTE_CARLO_CONFIG = Config(n0=5000, gamma=0.95, episodes=20, max_steps=50)
MONTE_CARLO_LFA_CONFIG = Config(n0=5000, gamma=0.95, episodes=15, max_steps=50)

SARSA_LAMBDA_CONFIG = Config(n0=500, gamma=0.95, lambd=0.8, episodes=20)
SARSA_LAMBDA_LFA_CONFIG = Config(n0=500, gamma=0.95, lambd=0.8, episodes=15)

Q_LEARNING_CONFIG = Config(n0=5000, gamma=0.95, episodes=200, max_steps=50)
Q_LEARNING_LFA_CONFIG = Config(n0=5000, gamma=0.95, episodes=15, max_steps=50)

# %%
def main(trainer, trainer_method: Method, cfg: Config) -> Any:
    np.random.seed(SEED)
    print(f"::: {trainer_method.value} :::")
    print(f"\n{cfg}\n")
    print(f"::: Training :::")
    return trainer.train(cfg)

# %%
class Color(Enum):
    RESET = '\033[0m'
    GREY = '\033[90m'  # debug
    RED = '\033[91m'  # error
    GREEN = '\033[92m'  # default
    YELLOW = '\033[93m'  # info
    BLUE = '\033[94m'
    MAGENTA = '\033[95m'  # warning
    CYAN = '\033[96m'
    WHITE = '\033[97m'

def _log_colored(color: Color, *args, **kwds):
    print(color.value, end="")
    print(*args, Color.RESET.value, **kwds)

from os import getenv


def log(*args, **kwds):
    if not getenv("NO_LOG"): _log_colored(Color.GREEN, "[LOG]", *args, **kwds)

def log_debug(*args, **kwds):
    if not getenv("NO_DEBUG"): _log_colored(Color.GREY, "[DEBUG]", *args, **kwds)

def log_warning(*args, **kwds):
    if not getenv("NO_WARNING"): _log_colored(Color.MAGENTA, "[WARNING]", *args, **kwds)

def log_error(*args, **kwds):
    if not getenv("NO_ERROR"): _log_colored(Color.RED, "[ERROR]", *args, **kwds)

def log_info(*args, **kwds):
    if not getenv("NO_INFO"): _log_colored(Color.YELLOW, "[INFO]", *args, **kwds)

# %%
# NOTE uncomment the log levels you don't want to see
os.environ["NO_LOG"] = "1"
# os.environ["NO_DEBUG"] = "1"
os.environ["NO_WARNING"] = "1"
# os.environ["NO_ERROR"] = "1"
# os.environ["NO_INFO"] = "1"

# NOTE set this to True to log step numbers (and False to hide it)
LOG_STEP_NUMBER = False


# %% ######################################################################### [markdown]
# # Código

# %% ######################################################################### [markdown]
# ## Agent

# %%
def unity_to_state(obs: np.ndarray) -> int:
    """Return the index of the state given a `UnityEnvironment` observation."""
    state = np.argmax(obs)
    return state


def unity_to_action(action_tuple: ActionTuple) -> int:
    """Return the index of the action given a `UnityEnvironment` ActionTuple."""
    assert action_tuple.discrete.shape == (1, 1)
    action = action_tuple.discrete[0, 0]
    return action


def action_to_unity(action: int) -> ActionTuple:
    """Builds an `ActionTuple` for a `UnityEnvironment` from the given action index."""
    action_tuple = ActionTuple()
    action_tuple.add_discrete(np.array(action).reshape((1, 1)))
    return action_tuple

# %%
def log_experience(state: int, action: int, reward: float, next_state: int) -> None:
    """Log a `(s, a, r, s')` tuple to console."""
    log(f"(s, a, r, s') = ({state}, {BASIC_ACTIONS[action]}, {reward:.2f}, {next_state})")

# %%
class Agent:
    """Base class for learning agents on the Basic ML-Agents environment."""

    def __init__(self, env: UnityEnvironment, cfg: Config) -> None:
        self.id = f"{cfg.n0}-{cfg.gamma}-{cfg.episodes}"

        self.env = env
        self.env.reset()
        if not self.env.behavior_specs:
            self.env.step()

        assert len(self.env.behavior_specs) == 1
        self.brain = list(self.env.behavior_specs)[0]
        self.behavior_spec = self.env.behavior_specs[self.brain]
        assert self.behavior_spec.action_spec.is_discrete()

        log(f"observation_shapes = {self.behavior_spec.observation_shapes}")
        log(f"action_spec = {self.behavior_spec.action_spec}")
        log(f"discrete_size = {self.behavior_spec.action_spec.discrete_size}")

        # Basic has 1 discrete action branch with 3 actions (do nothing,
        # move left, move right), and one variable corresponding to the
        # current state stored as a one-hot encoded 1 x 20 numpy array.
        assert self.behavior_spec.action_spec.discrete_branches == (3,)
        assert self.behavior_spec.action_spec.discrete_size == 1
        assert self.behavior_spec.observation_shapes == [(20,)]

        self._num_states = 20
        self._num_actions = 3
        self._N = np.zeros((self._num_states, self._num_actions),
                           dtype=np.int32)  # state-action visits counter
        self._N0 = cfg.n0
        self._gamma = cfg.gamma
        self._vis_freq = cfg.vis_freq
        self.Qs = []
        self.Ns = []

    def N(self, state: int, action: Optional[int] = None) -> int:
        """Return the number of times that:
        - State `s` has been visited, if `a is None`
        - Action `a` has been selected from state `s`, if `a is not None`
        """
        if action is None:
            return np.sum(self._N[state])
        return self._N[state, action]

    def alpha(self, state: int, action: int) -> float:
        """Return the (time-varying) learning rate `α = 1 / N(s,a)`."""
        return 1 / self.N(state, action)

    def epsilon(self, state: int) -> float:
        """Return the (time-varying) ε-greedy step size `ε = N0 / (N0 + N(s))`."""
        return self._N0 / (self._N0 + self.N(state))

    def _act(self, state: int, training: bool) -> int:
        """Determine the next action according to ε-greedy exloration strategy
        if `training is True`. Otherwise (i.e. we are evaluating), act greedy.
        """
        raise NotImplementedError

    def _update(self, state: int, action: int, reward: float, next_state: int,
                done: bool) -> None:
        """Update called after every state transition."""
        raise NotImplementedError

    def _at_episode_end(self, episode: int, max_episodes: int, training: bool) -> None:
        """Code to be called at the end of each episode."""
        raise NotImplementedError

    def reset(self) -> int:
        """Reset the environment and return an initial state."""
        self.env.reset()
        decision_steps, _ = self.env.get_steps(self.brain)
        assert len(decision_steps) == 1
        return unity_to_state(decision_steps.obs[0][0])

    def step(self, action: int) -> Tuple[float, int, bool]:
        """Run one timestep of the environment, returning `(reward, state, done)`."""
        action_tuple = action_to_unity(action)
        self.env.set_actions(self.brain, action_tuple)
        self.env.step()
        decision_steps, terminal_steps = self.env.get_steps(self.brain)
        assert bool(len(terminal_steps) == 1) != bool(len(decision_steps) == 1)
        if len(terminal_steps) == 0:
            reward, obs = decision_steps.reward[0], decision_steps.obs[0][0]
            return reward, unity_to_state(obs), False
        else:
            reward, obs = terminal_steps.reward[0], terminal_steps.obs[0][0]
            return reward, unity_to_state(obs), True

    def train(self, cfg: Config) -> Any:
        return self.run(cfg, training=True)

    def evaluate(self, cfg: Config) -> Any:
        episode_reward, episode_step = self.run(cfg, training=False)
        print(f"Avg. return over {cfg.episodes} episodes: {np.mean(episode_reward):.4f}")
        return episode_reward, episode_step

    def run(self, cfg: Config, training: bool = True) -> Any:
        episodes_reward = []
        episodes_step = []
        episodes_val_return = []

        assert not training or np.sum(self._N) == 0

        for episode in range(1, cfg.episodes + 1):
            if training:
                if cfg.max_steps is not None:
                    log_info(f"=== Episode {episode} / {cfg.episodes}"
                            f"  ({cfg.max_steps} max steps)")
                else:
                    log_info(f"=== Episode {episode} / {cfg.episodes}")

            step = 0
            done = False
            episode_reward = 0.0
            state = self.reset()

            while not done:
                if cfg.max_steps is not None and step >= cfg.max_steps:
                    break
                if LOG_STEP_NUMBER:
                    log_info(f"=== step {step}")

                action = self._act(state, training)
                reward, next_state, done = self.step(action)

                if training:
                    self._update(state, action, reward, next_state, done)
                log_experience(state, action, reward, next_state)

                step += 1
                episode_reward += reward
                state = next_state
            else:
                if training:
                    log_warning(f"Done! ({step} steps)")

            if training:
                val_return, _, _ = self.run(Config(episodes=1, max_steps=30),
                                            training=False)
                episodes_val_return.extend(val_return)

            episodes_reward.append(episode_reward)
            episodes_step.append(step)
            self._at_episode_end(episode, cfg.episodes, training)

        return episodes_reward, episodes_step, episodes_val_return

# %% ######################################################################### [markdown]
# ## Monte Carlo Control

# %%
class Episode:
    """Keep track of experiences for an episode."""

    def __init__(self, gamma):
        self._gamma = gamma
        self.rewards = []
        self.actions = []
        self.states = []
        self.next_states = []
        self.G = []

    def __len__(self):
        return len(self.rewards)

    def __iter__(self):
        for t in range(len(self)):
            yield self.states[t], self.actions[t], self.G[t]

    def append(self, state: int, action: int, reward: float, next_state: int) -> None:
        self.rewards.append(reward)
        self.actions.append(action)
        self.states.append(state)
        self.next_states.append(next_state)
        self.G.append(reward)
        G_update = reward
        for i in reversed(range(len(self.G) - 1)):
            G_update = self._gamma * G_update
            self.G[i] += G_update

    def clear(self):
        self.rewards = []
        self.actions = []
        self.states = []
        self.next_states = []
        self.G = []


class MonteCarlo(Agent):
    """Monte Carlo Control."""

    def __init__(self, env: UnityEnvironment, cfg: Config) -> None:
        super().__init__(env, cfg)
        self.id = f"mc-{self.id}"
        self._Q = np.zeros((self._num_states, self._num_actions), dtype=np.float64)
        self._episode = Episode(self._gamma)

    def _act(self, state: int, training: bool) -> int:
        """Determine the next action according to ε-greedy exloration strategy
        if `training is True`. Otherwise (i.e. we are evaluating), act greedy.
        """
        if training and np.random.rand() < self.epsilon(state):
            return unity_to_action(self.behavior_spec.action_spec.random_action(1))
        max_Q = np.max(self._Q[state])  # greedy action choice
        return np.random.choice(np.where(self._Q[state] == max_Q)[0])

    def _at_episode_end(self, episode: int, max_episodes: int, training: bool) -> None:
        """Update the Q value table using the entire episode."""
        visited = set()
        for state, action, G in self._episode:
            if (state, action) not in visited:
                visited.add((state, action))
                self._N[state, action] += 1
                alpha = self.alpha(state, action)
                error = G - self._Q[state, action]
                self._Q[state, action] += alpha * error
        self._episode.clear()
        if training and episode % self._vis_freq == 0:
            self.Qs.append(self._Q.copy())
            self.Ns.append(self._N.copy())

    def _update(self, state: int, action: int, reward: float, next_state: int,
                done: bool) -> None:
        """Keep track of the experiences for each step."""
        log_experience(state, action, reward, next_state)
        self._episode.append(state, action, reward, next_state)

# %% ######################################################################### [markdown]
# ## Monte Carlo Control (Linear Function Approximator)

# %%
class MonteCarloLinear(Agent):
    """Monte Carlo Control with linear function approximation."""

    def __init__(self, env: UnityEnvironment, cfg: Config) -> None:
        super().__init__(env, cfg)
        self.id = f"mc-lfa-{self.id}"
        self._w = np.random.normal(size=self._num_states + self._num_actions + 1)
        self._episode = Episode(self._gamma)

    def feature(self, state: int, action: int) -> np.ndarray:
        """Compute a feature from a state and action pair."""
        one_hot_state = np.zeros(self._num_states)
        one_hot_state[state] = 1
        one_hot_action = np.zeros(self._num_actions)
        one_hot_action[action] = 1
        return np.concatenate([one_hot_state, one_hot_action, [1]])

    def Q_value(self, state: int, action: Optional[int] = None) -> float:
        """Return the Q-value for the given state-action pair. If an action is provided,
        a single Q-value for the state-action pair is returned. Otherwise, a vector of
        all the Q-values for each action in a given state is returned."""
        if action is None:
            features = []
            for act in range(self._num_actions):
                features.append(self.feature(state, act))
            features = np.array(features)
            return np.dot(features, self._w)
        return np.dot(self.feature(state, action), self._w)

    def _act(self, state: int, training: bool) -> int:
        """Determine the next action according to ε-greedy exloration strategy
        if `training is True`. Otherwise (i.e. we are evaluating), act greedy.
        """
        if training and np.random.rand() < self.epsilon(state):
            return unity_to_action(self.behavior_spec.action_spec.random_action(1))
        Qs = self.Q_value(state)
        return np.argmax(Qs)  # greedy action choice

    def _at_episode_end(self, episode: int, max_episodes: int, training: bool) -> None:
        """Update the weight vector from the episode's experiences."""
        visited = set()
        for state, action, G in self._episode:
            if (state, action) not in visited:
                visited.add((state, action))
                self._N[state, action] += 1
                alpha = self.alpha(state, action)
                delta = G - self.Q_value(state, action)
                self._w += alpha * delta * self.feature(state, action)
        self._episode.clear()
        if training and episode % self._vis_freq == 0:
            Q = np.zeros(shape=(self._num_states, self._num_actions), dtype=np.float32)
            for state, action in np.ndindex(Q.shape):
                Q[state, action] = self.Q_value(state, action)
            self.Qs.append(Q)
            self.Ns.append(self._N.copy())

    def _update(self, state: int, action: int, reward: float, next_state: int,
                done: bool) -> None:
        """Keep track of the experiences for each step."""
        log_experience(state, action, reward, next_state)
        self._episode.append(state, action, reward, next_state)

# %% ######################################################################### [markdown]
# ## SARSA(λ)

# %%
class SarsaLambda(Agent):
    """SARSA(λ)."""

    def __init__(self, env: UnityEnvironment, cfg: Config) -> None:
        super().__init__(env, cfg)
        self.id = f"sarsalambda-{self.id}"
        self._Q = np.zeros((self._num_states, self._num_actions), dtype=np.float64)
        self._E = np.zeros((self._num_states, self._num_actions),
                           dtype=np.float64)  # state-action eligibility trace
        self._lambd = cfg.lambd

    def TD_error(self, state: int, action: int, reward: float, next_Q: float) -> float:
        """Return the TD-error `δ_t = (TD-target - Q(S_t, A_t))`.
        Where TD-target is `R_{t+1} + γ * Q(S_{t+1}, A_{t+1})`.
        """
        target = reward + self._gamma * next_Q
        error = target - self._Q[state, action]
        log(f"TD-target={target:.4f}, TD-error={error:.4f}")
        return error

    def _act(self, state: int, training: bool) -> int:
        if training and np.random.rand() < self.epsilon(state):
            return unity_to_action(self.behavior_spec.action_spec.random_action(1))
        max_Q = np.max(self._Q[state])  # greedy action choice
        return np.random.choice(np.where(self._Q[state] == max_Q)[0])

    def _update(self, state: int, action: int, reward: float, next_state: int,
                done: bool) -> None:
        log_experience(state, action, reward, next_state)

        # Update `_N`
        self._N[state, action] += 1

        alpha = self.alpha(state, action)

        next_action = self._act(next_state, training=True)  # NOTE on-policy
        next_Q = self._Q[next_state, next_action]
        if done: next_Q = 0.0
        error = self.TD_error(state, action, reward, next_Q)

        # Update `_E` and `_Q` according to SARSA(λ)
        self._E[state, action] += 1  # NOTE accumulating traces
        for s in range(self._num_states):
            for a in range(self._num_actions):
                self._Q[s, a] += alpha * error * self._E[s, a]
                self._E[s, a] *= self._gamma * self._lambd  # decay

    def _at_episode_end(self, episode: int, max_episodes: int, training: bool) -> None:
        if training:
            if episode % self._vis_freq == 0:
                self.Qs.append(self._Q.copy())
                self.Ns.append(self._N.copy())
            last_episode = episode == max_episodes
            log_on_last_episode = (lambda *args, **kwds: log_debug(*args, **kwds)
                                   if last_episode else None)
            # log_on_last_episode(f"E = \n{self._E}, ", end="")
            log_on_last_episode(f"Σ(E) = {np.sum(self._E):.4f}")
            # log_on_last_episode(f"N = \n{self._N}, ", end="")
            log_on_last_episode(f"Σ(N) = {np.sum(self._N)}")
            # log_on_last_episode(f"Q = \n{self._Q}")
            log_on_last_episode(f"Q(s=10) = {self._Q[10]}  (argmax = {np.argmax(self._Q[10])})")
            log_on_last_episode(f"argmax(Q) = {np.argmax(self._Q, axis=1)}"
                                "\n                                   ^     ^             ^"
                                "\n                                   7     10            17")


    def reset(self) -> int:
        # Reinitialize the eligibility traces to 0.0
        self._E = np.zeros((self._num_states, self._num_actions), dtype=np.float64)
        return super().reset()

    def run(self, cfg: Config, training: bool) -> Any:
        assert not training or np.sum(self._E) == 0.0
        assert not training or np.sum(self._Q) == 0.0
        return super().run(cfg, training=training)

# %% ######################################################################### [markdown]
# ## SARSA(λ) (Aproximador de Função Linear)

# %%
class SarsaLambdaLinear(Agent):
    """SARSA(λ) learning agent with linear function approximator."""

    def __init__(self, env: UnityEnvironment, cfg: Config) -> None:
        super().__init__(env, cfg)
        self.id = f"sarsalambda-lfa-{self.id}"
        self._w = np.random.normal(size=self._num_states + self._num_actions + 1)
        self._E = np.zeros(self._w.shape, dtype=np.float64)  # eligibility trace
        self._lambd = cfg.lambd

    def feature(self, state: int, action: int) -> np.ndarray:
        """Compute a feature from a state and action pair."""
        one_hot_state = np.zeros(self._num_states, dtype=np.int32)
        one_hot_state[state] = 1
        one_hot_action = np.zeros(self._num_actions, dtype=np.int32)
        one_hot_action[action] = 1
        return np.concatenate([one_hot_state, one_hot_action, [1]])

    def feature_indices(self, state: int, action: int) -> List[int]:
        """Return a list of indices where the computed feature values are equal to 1."""
        feat = self.feature(state, action)
        assert feat.dtype == np.int32
        return [i for i, _ in enumerate(feat) if _ == 1]

    def TD_error(self, state: int, action: int, reward: float, next_Q: float) -> float:
        """Return the TD-error `δ_t = (TD-target - Q(S_t, A_t))`.
        Where TD-target is `R_{t+1} + γ * Q(S_{t+1}, A_{t+1})`.
        """
        curr_Q = np.dot(self.feature(state, action), self._w)  # linear approximation
        target = reward + self._gamma * next_Q
        error = target - curr_Q
        log(f"TD-target={target:.4f}, TD-error={error:.4f}")
        return error

    def _act(self, state: int, training: bool) -> int:
        """Determine the next action according to ε-greedy exloration strategy
        if `training is True`. Otherwise (i.e. we are evaluating), act greedy.
        """
        if training and np.random.rand() < self.epsilon(state):
            return unity_to_action(self.behavior_spec.action_spec.random_action(1))
        features = np.array([self.feature(state, action)
                             for action in range(self._num_actions)])
        Qs = np.dot(features, self._w)  # linear approximation
        max_Q = np.max(Qs)  # greedy action choice
        return np.random.choice(np.where(Qs == max_Q)[0])

    def _update(self, state: int, action: int, reward: float, next_state: int,
                done: bool) -> None:
        log_experience(state, action, reward, next_state)
        self._N[state, action] += 1

        alpha = self.alpha(state, action)

        next_action = self._act(next_state, training=True)  # NOTE on-policy
        next_Q = np.dot(self.feature(next_state, next_action), self._w)  # linear approximation
        if done: next_Q = 0.0
        error = self.TD_error(state, action, reward, next_Q)

        # Update `_E` and `_w` according to SARSA(λ)
        self._E[self.feature_indices(state, action)] += 1  # NOTE accumulating traces
        self._w += alpha * error * self._E
        self._E *= self._gamma * self._lambd  # decay

    def _at_episode_end(self, episode: int, max_episodes: int, training: bool) -> None:
        """Log the values of `_w`, `_E` and `_N`."""
        if training:
            if episode % self._vis_freq == 0:
                Q = np.zeros(shape=(self._num_states, self._num_actions),
                             dtype=np.float32)
                for state, action in np.ndindex(Q.shape):
                    Q[state, action] = np.dot(self.feature(state, action), self._w)
                self.Qs.append(Q)
                self.Ns.append(self._N.copy())
            last_episode = episode == max_episodes
            log_on_last_episode = (lambda *args, **kwds: log_debug(*args, **kwds)
                                   if last_episode else None)
            # log_on_last_episode(f"E = \n{self._E}, ", end="")
            log_on_last_episode(f"Σ(E) = {np.sum(self._E):.4f}")
            # log_on_last_episode(f"N = \n{self._N}, ", end="")
            log_on_last_episode(f"Σ(N) = {np.sum(self._N)}")
            log_on_last_episode(f"w = {self._w}")
            if last_episode:
                argmax_q_hat = []
                for s in range(self._num_states):
                    features = np.array([self.feature(s, a)
                                         for a in range(self._num_actions)])
                    argmax_q_hat.append(np.argmax(np.dot(features, self._w)))
                log_on_last_episode(f"argmax(q̂) = {np.array(argmax_q_hat, dtype=np.int32)}"
                                    "\n                                   ^     ^             ^"
                                    "\n                                   7     10            17")

    def reset(self) -> int:
        """Reset the environment and return an initial state."""
        # Reinitialize the eligibility traces to 0.0
        self._E = np.zeros(self._w.shape, dtype=np.float64)
        return super().reset()

    def run(self, cfg: Config, training: bool) -> Any:
        assert not training or np.sum(self._E) == 0.0
        return super().run(cfg, training=training)

# %% ######################################################################### [markdown]
# ## Q-Learning

# %%
class QLearning(Agent):
    """Q-Learning."""

    def __init__(self, env: UnityEnvironment, cfg: Config) -> None:
        super().__init__(env, cfg)
        self.id = f"qlearn-{self.id}"
        self._Q = np.zeros((self._num_states, self._num_actions), dtype=np.float64)

    def TD_error(self, state: int, action: int, reward: float, next_state: int,
                 done: bool) -> float:
        """Return the TD-error `(δ_t - Q(S_t, A_t))`, where `δ_t` is the TD-target."""
        next_Q = self.Q_value(next_state) if not done else 0.0
        target = reward + self._gamma * next_Q
        return target - self.Q_value(state, action)

    def _act(self, state: int, training: bool) -> int:
        """Determine the next action according to ε-greedy exloration strategy
        if `training is True`. Otherwise (i.e. we are evaluating), act greedy.
        """
        if training and np.random.rand() < self.epsilon(state):
            return unity_to_action(self.behavior_spec.action_spec.random_action(1))
        max_Q = self.Q_value(state)  # greedy action choice
        return np.random.choice(np.where(self._Q[state] == max_Q)[0])

    def Q_value(self, state: int, action: Optional[int] = None) -> float:
        """Return the Q-value for the given state-action pair."""
        if action is None:
            return np.max(self._Q[state])
        return self._Q[state, action]

    def _at_episode_end(self, episode: int, max_episodes: int, training: bool) -> None:
        """Add visualizations."""
        if training and episode % self._vis_freq == 0:
            self.Qs.append(self._Q.copy())
            self.Ns.append(self._N.copy())

    def _update(self, state: int, action: int, reward: float, next_state: int,
                done: bool) -> None:
        """Update the values of `Q` and `N`."""
        self._N[state, action] += 1
        log_experience(state, action, reward, next_state)
        delta = self.TD_error(state, action, reward, next_state, done)
        alpha = self.alpha(state, action)
        log(f"α={alpha}, TD-error={delta:.4f}")
        self._Q[state, action] += alpha * delta

# %% ######################################################################### [markdown]
# ## Q-Learning (Aproximador de Função Linear)

# %%
class QLearningLinear(Agent):
    """Q-Learning with linear approximation."""

    def __init__(self, env: UnityEnvironment, cfg: Config) -> None:
        super().__init__(env, cfg)
        self.id = f"qlearn-lfa-{self.id}"
        self._w = np.random.normal(size=self._num_states + self._num_actions + 1)

    def TD_error(self, state: int, action: int, reward: float, next_state: int,
                 done: bool) -> float:
        """Return the TD-error `(δ_t - Q(S_t, A_t))`, where `δ_t` is the TD-target."""
        next_Q = np.max(self.Q_value(next_state)) if not done else 0.0
        target = reward + self._gamma * next_Q
        return target - self.Q_value(state, action)

    def _act(self, state: int, training: bool) -> int:
        """Determine the next action according to ε-greedy exloration strategy
        if `training is True`. Otherwise (i.e. we are evaluating), act greedy.
        """
        if training and np.random.rand() < self.epsilon(state):
            return unity_to_action(self.behavior_spec.action_spec.random_action(1))
        Qs = self.Q_value(state)
        return np.argmax(Qs)  # greedy action choice

    def feature(self, state: int, action: int) -> np.ndarray:
        """Compute a feature from a state and action pair."""
        one_hot_state = np.zeros(self._num_states)
        one_hot_state[state] = 1
        one_hot_action = np.zeros(self._num_actions)
        one_hot_action[action] = 1
        return np.concatenate([one_hot_state, one_hot_action, [1]])

    def Q_value(self, state: int, action: Optional[int] = None) -> float:
        """Return the Q-value for the given state-action pair. If an action is provided,
        a single Q-value for the state-action pair is returned. Otherwise, a vector of
        all the Q-values for each action in a given state is returned."""
        if action is None:
            features = []
            for act in range(self._num_actions):
                features.append(self.feature(state, act))
            features = np.array(features)
            return np.dot(features, self._w)
        return np.dot(self.feature(state, action), self._w)

    def _at_episode_end(self, episode: int, max_episodes: int, training: bool) -> None:
        """Don't do anything for now."""
        if training and episode % self._vis_freq == 0:
            Q = np.zeros(shape=(self._num_states, self._num_actions), dtype=np.float32)
            for state, action in np.ndindex(Q.shape):
                Q[state, action] = self.Q_value(state, action)
            self.Qs.append(Q)
            self.Ns.append(self._N.copy())

    def _update(self, state: int, action: int, reward: float, next_state: int,
                done: bool) -> None:
        """Update the values of `w` and `N`."""
        self._N[state, action] += 1
        log_experience(state, action, reward, next_state)
        delta = self.TD_error(state, action, reward, next_state, done)
        alpha = self.alpha(state, action)
        log(f"α={alpha}, TD-error={delta:.4f}")
        self._w += alpha * delta * self.feature(state, action)

# %% ######################################################################### [markdown]
# ## Visualização

# %%
action_to_arrow = {
    1: {
        'x': 0.25,
        'y': 0.5,
        'dx': -0.5,
        'dy': 0.0
    },
    2: {
        'x': -0.25,
        'y': 0.5,
        'dx': 0.5,
        'dy': 0.0
    },
}


def plot_returns(returns):
    fig, ax = plt.subplots()
    ax.set_xlabel("Episódio")
    ax.set_ylabel("$G$")
    xs = np.arange(1, len(returns) + 1)
    ax.scatter(xs, returns, alpha=0.8)


def vis_Qs(Qs, vis_freq, fps=2):
    episode = vis_freq
    fig, ax = plt.subplots()
    for Q in Qs:
        ax.cla()
        ax.set_title(f"Episódio {episode}")
        ax.set_xlim(-0.5, len(Qs[0]) - 0.5)
        ax.set_yticks([])
        ax.set_xticks(np.arange(0, len(Qs[0]), 1))
        V_a = np.argmax(Q, axis=1)
        V = np.max(Q, axis=1, keepdims=True).T
        ax.imshow(V, cmap="inferno", extent=[-0.5, len(Q) - 0.5, 0, 1])
        for (i, j), _ in np.ndenumerate(V):
            action = V_a[j]
            if j in (7, 17):
                circle = Circle((j, 0.5), radius=j / 50, color="g", alpha=0.7)
                ax.add_patch(circle)
            elif action == 0:
                circle = Circle((j, 0.5), radius=0.1, color="r", alpha=0.7)
                ax.add_patch(circle)
            else:
                x, y, dx, dy = action_to_arrow[action].values()
                ax.arrow(x=j + x,
                         y=i + y,
                         dx=dx,
                         dy=dy,
                         length_includes_head=True,
                         head_width=0.1,
                         head_length=0.1,
                         color='r')
        display(fig)
        clear_output(wait=True)
        plt.pause(1 / fps)
        episode += vis_freq


def vis_Ns(Ns, vis_freq, fps=2):
    episode = vis_freq
    fig, ax = plt.subplots()
    for N in Ns:
        ax.cla()
        ax.set_title(f"Episódio {episode}")
        ax.imshow(N.T,
                  origin="lower",
                  cmap="magma",
                  extent=[-0.5, len(N) - 0.5, -0.5, 2.5])
        ax.set_xticks(np.arange(0, len(N)))
        ax.set_yticks(np.arange(0, len(N.T)))
        ax.set_yticklabels(BASIC_ACTIONS.values(), rotation=20)

        # Draw the number of times each (s,a) was visited
        black_label_thresh = np.max(N) * 0.8
        for (j, i), label in np.ndenumerate(N.T):
            color = "black" if label > black_label_thresh else "white"
            ax.text(i, j, label, ha="center", va="center", color=color)
        fig.tight_layout()
        display(fig)
        clear_output(wait=True)
        plt.pause(1 / fps)
        episode += vis_freq

# %% ######################################################################### [markdown]
# # Relatório

# %% ######################################################################### [markdown]
# ## Problema
# O problema abordado é baseado no _environment_ `Basic` da biblioteca [ML-Agents](https://github.com/Unity-Technologies/ml-agents) da Unity:
#
# ![](https://raw.githubusercontent.com/Unity-Technologies/ml-agents/master/docs/images/basic.png)
#
# Nele, temos um agente cujas ações são: ficar parado, mover-se para a esquerda, e mover-se para a direita. Há 20 estados (correspondentes às posições `x`), representados com _one-hot encoding_, sendo que o agente começa na posição 10 e há dois estados terminais: posições 7 e 17.
#
# A cada passo o agente ganha um _reward_ de -0.01, e quando ele atinge os estados terminais 7 e 17 ele recebe +0.1 e +1.0, respectivamente.
#
# Assim, como não há aleatoriedade no ambiente, esperamos que os métodos cheguem no ótimo retorno esperado de 0.93, i.e., partindo da posição 10, sempre tomar a ação "ir para a direita" (ganhando então +1.0 e 7 * -0.01).
#


# %% ######################################################################### [markdown]
# ## Métodos tabulares

# %% ######################################################################### [markdown]
# ### Monte Carlo Control

# %%
MONTE_CARLO_CONFIG = Config(n0=50, gamma=0.95, episodes=20, max_steps=50)

# %%
with unity_env(BASIC_ENV, no_graphics=True, seed=SEED) as env:
    cfg = MONTE_CARLO_CONFIG
    trainer = MonteCarlo(env, cfg)
    _, _, val_returns = main(trainer, Method.MONTE_CARLO, cfg)

# %%
plot_returns(val_returns)


# %%
vis_Qs(trainer.Qs, cfg.vis_freq)

# %%
vis_Ns(trainer.Ns, cfg.vis_freq)

# %%
MONTE_CARLO_CONFIG = Config(n0=5000, gamma=0.95, episodes=20, max_steps=50)

# %%
with unity_env(BASIC_ENV, no_graphics=True, seed=SEED) as env:
    cfg = MONTE_CARLO_CONFIG
    trainer = MonteCarlo(env, cfg)
    _, _, val_returns = main(trainer, Method.MONTE_CARLO, cfg)

# %%
plot_returns(val_returns)

# %% [markdown]
# ### Q-learning

# %% [markdown]
# Para o Q-learning, o *trade-off* de exploração e aplicação (*exploitation*) é muito mais crítico do que no Monte Carlo, já que fazemos um ajuste da estimativa da função estado-valor a cada passo. Isso faz com que nos primeiros episódios, o agente atualize a tabela Q com uma estimativa incorreta do retorno para o par estado-valor, visto que ele ainda não chegou no estado terminal, que tem a maior parte da recompensa. O Q-learning precisa de mais tempo para propagar as recompensas obtidas nos estados terminais para o inicial. Como o ambiente utilizado é determinístico e o espaço de estados e ações é pequeno, o *trade-off* de exploração e aplicação não é tão difícil de ajustar, visto que o algoritmo seria capaz de aprender a política ótima se a gente sempre priorizasse a exploração. A aplicação é mais crítica quando o espaço de estados e ações é muito grande e não é possível vistar todos os estados e ações do problema em um tempo razoável.
#
# Para visualizar o *trade-off*, rodamos três experimentos variando o valor de $N_0 \in \{50, 500, 5000\}$ de modo a ajustar a taxa de exploração da política $\epsilon$-greedy durante o treinamento. Quanto maior o $N_0$, mais devagar irá decair o valor de $\epsilon$, priorizando a exploração sobre a aplicação.
#
# Para $N_0 = 50$, temos o experimento abaixo:


# %% ####################################################################################
with unity_env(BASIC_ENV, no_graphics=True, seed=SEED) as env:
    cfg = Q_LEARNING_CONFIG
    cfg = cfg._replace(n0=50)
    trainer = QLearning(env, cfg)
    _, _, val_returns = main(trainer, Method.Q_LEARNING, cfg)

# %%
plot_returns(val_returns)

# %%
vis_Qs(trainer.Qs, cfg.vis_freq)

# %%
vis_Ns(trainer.Ns, cfg.vis_freq)

# %% [markdown]
# Note como o agente não converge para a solução ótima e fica preso em um ótimo local, dado pela recompensa menor na posição 7. Isso acontece porque o $\epsilon$ decai muito rápido e o agente passa a adotar uma política gulosa que nunca mais tenta explorar os estados à direita. Na visualização da tabela $N$, podemos ver como os estados entre 11 e 17 são visitados cerca de 5 vezes, e o algoritmo fica preso visitando somente o estados à esquerda do ponto inicial.
#
# Para $N_0 = 500$, temos o treinamento

# %%
with unity_env(BASIC_ENV, no_graphics=True, seed=SEED) as env:
    cfg = Q_LEARNING_CONFIG
    cfg = cfg._replace(n0=500)
    trainer = QLearning(env, cfg)
    _, _, val_returns = main(trainer, Method.Q_LEARNING, cfg)

# %%
plot_returns(val_returns)

# %%
vis_Qs(trainer.Qs, cfg.vis_freq, fps=10)

# %%
vis_Ns(trainer.Ns, cfg.vis_freq, fps=10)

# %% [markdown]
# Aumentando o valor de $N_0$, foi possível observar uma melhora na exploração do agente, que passa a visitar mais vezes os estados à direita do inicial. Apesar do agente aprender que nos estados 11 a 16 ele deve ir para direita, a taxa de exploração não foi suficientemente alta para dar tempo de propagar o retorno esperado da ação ir para direita para ação 10 e 11, fazendo com que o agente continue preso em um mínimo local.
#
# Finalmente, para $N_0 = 5000$, temos o treinamento

# %%
with unity_env(BASIC_ENV, no_graphics=True, seed=SEED) as env:
    cfg = Q_LEARNING_CONFIG
    cfg = cfg._replace(n0=5000)
    trainer = QLearning(env, cfg)
    _, _, val_returns = main(trainer, Method.Q_LEARNING, cfg)

# %%
plot_returns(val_returns)

# %%
vis_Qs(trainer.Qs, cfg.vis_freq, fps=10)

# %%
vis_Ns(trainer.Ns, cfg.vis_freq, fps=10)

# %% [markdown]
# Com $N_0 = 5000$ obtivemos uma configuração que converge para solução ótima, obtendo um retorno de 0,93 a partir da época 175. Note como a função valor não aprende que os  estados à esquerda do ponto inicial também devem ir para direita, já que a taxa de exploração ainda não é alta o suficiente para isso. Se aumentássemos ainda mais o valor de $N_0$, esperaríamos que para todos os estados entre 7 e 17, a ação ótima seja ir para direita.

# %% [markdown]
# ### SARSA(λ)

# %% ######################################################################### [markdown]
# λ = 0.9
# %%
with unity_env(BASIC_ENV, no_graphics=True, seed=SEED) as env:
    cfg = SARSA_LAMBDA_CONFIG
    cfg = cfg._replace(lambd=0.9)
    trainer = SarsaLambda(env, cfg)
    main(trainer, Method.SARSA_LAMBDA, cfg)

# %%
plot_returns(val_returns)

# %%
vis_Qs(trainer.Qs, cfg.vis_freq)

# %%
vis_Ns(trainer.Ns, cfg.vis_freq)

# %% ######################################################################### [markdown]
# λ = 0.8
# %%
with unity_env(BASIC_ENV, no_graphics=True, seed=SEED) as env:
    cfg = SARSA_LAMBDA_CONFIG
    assert cfg.lambd == 0.8
    trainer = SarsaLambda(env, cfg)
    main(trainer, Method.SARSA_LAMBDA, cfg)

# %%
plot_returns(val_returns)

# %%
vis_Qs(trainer.Qs, cfg.vis_freq)

# %%
vis_Ns(trainer.Ns, cfg.vis_freq)

# %% ######################################################################### [markdown]
# λ = 0.7
# %%
with unity_env(BASIC_ENV, no_graphics=True, seed=SEED) as env:
    cfg = SARSA_LAMBDA_CONFIG
    cfg = cfg._replace(lambd=0.7)
    trainer = SarsaLambda(env, cfg)
    main(trainer, Method.SARSA_LAMBDA, cfg)

# %%
plot_returns(val_returns)

# %%
vis_Qs(trainer.Qs, cfg.vis_freq)

# %%
vis_Ns(trainer.Ns, cfg.vis_freq)

# %% ######################################################################### [markdown]
# λ = 0.6
# %%
with unity_env(BASIC_ENV, no_graphics=True, seed=SEED) as env:
    cfg = SARSA_LAMBDA_CONFIG
    cfg = cfg._replace(lambd=0.6)
    trainer = SarsaLambda(env, cfg)
    main(trainer, Method.SARSA_LAMBDA, cfg)

# %%
plot_returns(val_returns)

# %%
vis_Qs(trainer.Qs, cfg.vis_freq)

# %%
vis_Ns(trainer.Ns, cfg.vis_freq)

# %% ######################################################################### [markdown]
# λ = 0.2
# %%
with unity_env(BASIC_ENV, no_graphics=True, seed=SEED) as env:
    cfg = SARSA_LAMBDA_CONFIG
    cfg = cfg._replace(lambd=0.2)
    trainer = SarsaLambda(env, cfg)
    main(trainer, Method.SARSA_LAMBDA, cfg)

# %%
plot_returns(val_returns)

# %%
vis_Qs(trainer.Qs, cfg.vis_freq)

# %%
vis_Ns(trainer.Ns, cfg.vis_freq)

# %% ######################################################################### [markdown]
# λ = 0.0
# %%
with unity_env(BASIC_ENV, no_graphics=True, seed=SEED) as env:
    cfg = SARSA_LAMBDA_CONFIG
    cfg = cfg._replace(lambd=0.0)
    trainer = SarsaLambda(env, cfg)
    main(trainer, Method.SARSA_LAMBDA, cfg)

# %%
plot_returns(val_returns)

# %%
vis_Qs(trainer.Qs, cfg.vis_freq)

# %%
vis_Ns(trainer.Ns, cfg.vis_freq)

# %% ######################################################################### [markdown]
# ### Q-Learning

# %%
with unity_env(BASIC_ENV, no_graphics=True, seed=SEED) as env:
    cfg = Q_LEARNING_LFA_CONFIG
    trainer = QLearningLinear(env, cfg)
    main(trainer, Method.Q_LEARNING, cfg)

# %% ######################################################################### [markdown]
# ## Métodos com aproximador de função linear

# %% [markdown]
# Para os métodos com aproximação linear usamos como vetor de features $f(\mathbf{x})$ a codificação *one-hot* dos estados e das ações, resultando em um vetor de $20 + 3$ posições. Os pesos $\mathbf{w}$, foram inicializados com a normalização $z$-score e têm dimensão $24$, uma a mais que vetor de features devido ao termo do viés. Usando os pesos $\mathbf{w}$ para aproximar a nossa função estado-ação $Q$, adaptamos todos os algoritmos tabulares apresentados na seção anterior para usarem um aproximador de função linear no lugar de $Q$.
#
# Devido a simplicidade do problema, os pesos associados às features dos estados é irrelevante para o na nossa modelagem, visto que o agente só precisa aprender a sempre ir para direita, independente do estado. Por isso, nos gráficos do retorno obtido com a política gulosa, deve-se atentar ao tempo para o algoritmo se estabilizar na solução ótima, já que ele vai oscilar entre ir sempre para esquerda, ficar parado ou sempre para direita.

# %% ######################################################################### [markdown]
# ### Monte Carlo Control

# %% ####################################################################################
with unity_env(BASIC_ENV, no_graphics=True, seed=SEED) as env:
    cfg = MONTE_CARLO_LFA_CONFIG
    trainer = MonteCarlo(env, cfg)
    _, _, val_returns = main(trainer, Method.MONTE_CARLO_LFA, cfg)

# %%
plot_returns(val_returns)

# %%
vis_Qs(trainer.Qs, cfg.vis_freq)

# %%
vis_Ns(trainer.Ns, cfg.vis_freq)

# %% ######################################################################### [markdown]
# ### SARSA(λ)

# %% ######################################################################### [markdown]
# λ = 0.9
# %%
with unity_env(BASIC_ENV, no_graphics=True, seed=SEED) as env:
    cfg = SARSA_LAMBDA_LFA_CONFIG
    cfg = cfg._replace(lambd=0.9)
    trainer = SarsaLambdaLinear(env, cfg)
    main(trainer, Method.SARSA_LAMBDA, cfg)

# %%
plot_returns(val_returns)

# %%
vis_Qs(trainer.Qs, cfg.vis_freq)

# %%
vis_Ns(trainer.Ns, cfg.vis_freq)

# %% ######################################################################### [markdown]
# λ = 0.8
# %%
with unity_env(BASIC_ENV, no_graphics=True, seed=SEED) as env:
    cfg = SARSA_LAMBDA_LFA_CONFIG
    assert cfg.lambd == 0.8
    trainer = SarsaLambdaLinear(env, cfg)
    main(trainer, Method.SARSA_LAMBDA_LFA, cfg)

# %%
plot_returns(val_returns)

# %%
vis_Qs(trainer.Qs, cfg.vis_freq)

# %%
vis_Ns(trainer.Ns, cfg.vis_freq)

# %% ######################################################################### [markdown]
# λ = 0.7
# %%
with unity_env(BASIC_ENV, no_graphics=True, seed=SEED) as env:
    cfg = SARSA_LAMBDA_LFA_CONFIG
    cfg = cfg._replace(lambd=0.7)
    trainer = SarsaLambdaLinear(env, cfg)
    main(trainer, Method.SARSA_LAMBDA_LFA, cfg)

# %%
plot_returns(val_returns)

# %%
vis_Qs(trainer.Qs, cfg.vis_freq)

# %%
vis_Ns(trainer.Ns, cfg.vis_freq)

# %% ######################################################################### [markdown]
# λ = 0.6
# %%
with unity_env(BASIC_ENV, no_graphics=True, seed=SEED) as env:
    cfg = SARSA_LAMBDA_LFA_CONFIG
    cfg = cfg._replace(lambd=0.6)
    trainer = SarsaLambdaLinear(env, cfg)
    main(trainer, Method.SARSA_LAMBDA_LFA, cfg)

# %%
plot_returns(val_returns)

# %%
vis_Qs(trainer.Qs, cfg.vis_freq)

# %%
vis_Ns(trainer.Ns, cfg.vis_freq)

# %% ######################################################################### [markdown]
# λ = 0.2
# %%
with unity_env(BASIC_ENV, no_graphics=True, seed=SEED) as env:
    cfg = SARSA_LAMBDA_LFA_CONFIG
    cfg = cfg._replace(lambd=0.2)
    trainer = SarsaLambdaLinear(env, cfg)
    main(trainer, Method.SARSA_LAMBDA_LFA, cfg)

# %%
plot_returns(val_returns)

# %%
vis_Qs(trainer.Qs, cfg.vis_freq)

# %%
vis_Ns(trainer.Ns, cfg.vis_freq)

# %% ######################################################################### [markdown]
# λ = 0.0
# %%
with unity_env(BASIC_ENV, no_graphics=True, seed=SEED) as env:
    cfg = SARSA_LAMBDA_LFA_CONFIG
    cfg = cfg._replace(lambd=0.0)
    trainer = SarsaLambdaLinear(env, cfg)
    _, _, val_returns = main(trainer, Method.SARSA_LAMBDA_LFA, cfg)

# %%
plot_returns(val_returns)

# %%
vis_Qs(trainer.Qs, cfg.vis_freq)

# %%
vis_Ns(trainer.Ns, cfg.vis_freq)

# %% ######################################################################### [markdown]
# ### Q-Learning

# %% [markdown]
# Para o Q-learning com aproximador de função linear, obtivemos resultados semelhantes ao Monte Carlo, obtendo uma convergência estável em cerca de 15 episódios.

# %% ####################################################################################

with unity_env(BASIC_ENV, no_graphics=True, seed=SEED) as env:
    cfg = Q_LEARNING_LFA_CONFIG
    cfg = cfg._replace(episodes=15)
    trainer = QLearningLinear(env, cfg)
    _, _, val_returns = main(trainer, Method.Q_LEARNING_LFA, cfg)

# %%
plot_returns(val_returns)

# %%
vis_Qs(trainer.Qs, cfg.vis_freq)

# %%
vis_Ns(trainer.Ns, cfg.vis_freq)

# %% ######################################################################### [markdown]
# ## Discussão

# %% ########################################################################## [markdown]
# ### Monte Carlo
#
# Os métodos de Monte Carlo, são uma ampla classe de algoritmos computacionais que dependem de amostragem aleatória
# repetida para obter resultados numéricos. O conceito subjacente é usar a aleatoriedade para resolver problemas
# que podem ser determinísticos em princípio. A estimativa de Monte Carlo pode ser usada no controle, ou seja, para aproximar as políticas ótimas. A ideia geral é que a função valor seja alterada repetidamente para se aproximar mais da função valor para a política atual, e a política é repetidamente melhorada. Um pseudocódigo para um controlador Monte Carlo é dado por:
# ```
#     Monte-Carlo On-Policy:
#         Q(s,a) = 0, N(s,a) = 0 ∀(s,a)
#         π_k = ϵ-greedy(Q)
#
#         repeat (for each episode)
#             Sample episode
#             G_k_t = R_k_t + gamma * R_k_t-1 + ... + gamma ^ (T-1) * R_k_t
#
#             for (t = 1, ..., T) do
#                 if first_visit to (s,a) then
#                     N(s,a) += 1
#                     Q(s_t, a_t) += alpha * (G_k_t - Q(s_t, a_t))
#                 end if
#             end
#
#         until last episode
# ```
#
# Para a implementação tabular apresentada após $10$ episódios, obtivemos um retorno médio de 0.93, este é, inclusive, o valor de benchmark apresentado no projeto do ML Agents, mostrando que o resultado encontrado e a implementação foram bem sucedidas. A configuração adotada para o Monte Carlo foi um $N_0 = 5000$, $\gamma = 0.95$, $\lambda = 0.6$ e um total de $20$ episódios. Esse é um ponto a partir do qual podemos tirar algumas conclusões interessantes, em primeiro lugar, uma vez que o problema abordado foi bastante simples, foi possível observar uma convergência do algoritmo mesmo com um número baixo de episódios, tal comportamento, contudo, pode não ser possível para problemas mais complexos, nestes um número maior de episódios será necessário. Outro ponto interessante de notar sobre a configuração inicial é com relação a como o problema se comporta à medida que alteramos o valor de $N_0$, o retorno médio obtido é o praticamente o mesmo independente de usarmos $N_0 = 50$ ou $N_0 = 5000$, portanto o algoritmo é pouco sensível a tal valor.
#
# Também podemos notar que, uma vez que nosso problema é bastante simples e determinístico, o algoritmo de Monte Carlo obteve alguma vantagem, até mesmo quando comparado a outros que normalmente são melhores do que ele, como o Q-Learning, o principal motivo que justifica essa situação está no fato de que para o Monte Carlo basta que ele movimente o agente uma única vez para o lado direito para aprender que aquele é um bom estado e então continuar movimentando o agente para lá. Se observarmos, por exemplo, o gráfico de retornos obtidos, é possível perceber que após o sétimo episódio os experimentos não obtém mais retornos negativos e a maioria deles se concentra próximo de $0.9$.

# %% ######################################################################### [markdown]
# ### SARSA(λ)
# ```
#  1. Initialize Q(s,a) arbitrarily, for all s ϵ S, a ϵ A(s)
#  2. Repeat (for each episode):
#  3.     E(s,a) = 0, for all s ϵ S, a ϵ A(s)
#  4.     Initialize S, A
#  5.     Repeat (for each step of episode):
#  6.         Take action A, observe R, S'
#  7.         Choose A' from S' using policy derived from Q (e.g., ε-greedy)
#  8.         δ ← R + γ Q(S',A') - Q(S,A)
#  9.         E(S,A) ← E(S,A) + 1
# 10.         For all s ϵ S, a ϵ A(s):
# 11.             Q(s,a) ← Q(s,a) + α δ E(s,a)
# 12.             E(s,a) ← γ λ E(s,a)
# 13.         S ← S'; A ← A'
# 14.     until S is terminal
# ```
#
# No algoritmo (_backward view_) SARSA(λ) tabular guardamos uma tabela _eligibility traces_ para cara para par estado-ação, de modo a ponderar a atualização dos _Q-values_ baseado em quão recente foi a visita a um dado par.
# Ou seja, cada vez que visitamos um dado par `(S,A)` o valor de `E(S,A)` é aumentado (linha 9.), e então começa a decair (já que a cada passo de um episódio *todos* os valores das de `Q` e `E` são atualizados, conforme as linhas 10. a 12.). Já quando utilizamos _features_ (binárias) e vetor de pesos (`w`) para aproximar os valores de ação (_Q-values_), passamos a atualizar o `w` (ao invés da tabela `Q`).
#
# Diferente de Q-Learning, o método SARSA(λ) é _on-policy_ pois utilizamos a própria política para determinar as ações seguintes para o cálculo do _target_ (linhas 7. e 8.), e, consequentemente, para atualizar nossas estimativas.
#
# Analisando a influência de diferentes valores para λ, vemos que quando λ = 0.0 o algoritmo se reduz ao SARSA:
# ```
#  1. Initialize Q(s,a) arbitrarily, for all s ϵ S, a ϵ A(s)
#  2. Repeat (for each episode):
#  3.     Initialize S, A
#  4.     Repeat (for each step of episode):
#  5.         Take action A, observe R, S'
#  6.         Choose A' from S' using policy derived from Q (e.g., ε-greedy)
#  9.         Q(S,A) ← Q(S,A) + α [R + γ Q(S',A') - Q(S,A)]
# 10.         S ← S'; A ← A'
# 11.     until S is terminal
# ```
# de modo que atualizamos apenas o _Q-value_ para o estado-ação imediato (i.e. não "lembramos" dos pares recentes). Enquanto que, quanto maior for o valor de λ, por mais tempo uma entrada de `Q` é atualizada (pois, pela linha 11. do algoritmo original, demora mais para `E(s,a)` chegar em 0.0), proporcional ao _TD-error_ de estados futuros próximos.
#
# Vemos pelos resultados que apenas valores altos de λ (> 0.8) levam o agente a aprender que a melhor ação é ir sempre para a direita (ação de valor 2), rumo à posição 17.
#

# %% ######################################################################### [markdown]
# # Contribuições
# - Christian Maekawa (231867): aproximadores não lineares de função (p.ex. redes neurais) e discussão.
# - Felipe Tavares (265680): -
# - Luiz Eduardo Cartolano (183012): implementação do controlador Monte Carlo (tabular e com aproximador).
# - Rafael Prudencio (186145): implementação do Q-learning (aproximador), Monte Carlo (tabular e com aproximador) e visualizações.
# - Tiago Chaves (187690): implementação do SARSA(λ) (tabular e com aproximador), Q-Learning tabular, e base para Monte Carlo tabular.
