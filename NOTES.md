<!-- Feel free to add more items in here: -->
# Troubleshooting

* The Windows version of PyTorch doesn't work from PyPi (i.e. `pip install pytorch==1.7.0`), see [[this]](https://github.com/Unity-Technologies/ml-agents/blob/master/docs/Installation.md#windows-installing-pytorch)

* If you have cloned the `project-1` repository but it doesn't have a `ml-agents` folder inside, simply run:
  ```
  $ git submodule update --init
  ```

* In case you get any GitLab credential errors, you can try using HTTPS instead of SSH by changing the `url =` values in the `.git/config` file, e.g.:
  ```
  [remote "origin"]
      url = https://<your_git_username>@gitlab.com/unicamp016/mc935/project-1.git
  ...
  [submodule "ml-agents"]
      active = true
      url = https://<your_git_username>@gitlab.com/unicamp016/mc935/ml-agents.git
  ```

* For instructions on how to build an environment (e.g. to use it with `main.py`), see [[this]](https://github.com/Unity-Technologies/ml-agents/blob/master/docs/Learning-Environment-Executable.md)

<!-- Feel free to add more items in here: -->
# Algorithms

<!-- ## Monte Carlo (Exploring Starts)
```
Initialize, for all s ϵ S, a ϵ A(s):
    Q(s,a) ← arbitrary
    π(s) ← arbitrary
    Returns(s,a) ← empty list
Repeat forever (for each episode):
    Choose S0 ∈ S and A0 ∈ A(S0) randomly s.t. all pairs have probability > 0
    Generate an episode starting from S0,A0, following π
    G ← 0
    For each pair s,a appearing in the episode:
        G ← return following the first occurrence of s,a
        Append G to Returns(s,a)
        Q(s,a) ← average(Returns(s,a))
    For each s in the episode:
        π(s) ← argmax_a{Q(s,a)}
``` -->

<!-- ## SARSA (on-policy TD control)
https://www.davidsilver.uk/wp-content/uploads/2020/03/control.pdf slide 22
```
Initialize Q(s,a), for all s ϵ S, a ϵ A(s), arbitrarily, and Q(terminal-state, ·) = 0
Repeat (for each episode):
    Initialize S
    Choose A from S using policy derived from Q (e.g., ε-greedy)
    Repeat (for each step of episode):
        Take action A, observe R, S'
        Choose A' from S' using policy derived from Q (e.g., ε-greedy)
        Q(S,A) ← Q(S,A) + α [R + γ Q(S',A') - Q(S,A)]
        S ← S'; A ← A'
    until S is terminal
``` -->

## SARSA(λ)
<!-- https://www.davidsilver.uk/wp-content/uploads/2020/03/control.pdf slide 29 -->
```
Initialize Q(s,a) arbitrarily, for all s ϵ S, a ϵ A(s)
Repeat (for each episode):
    E(s,a) = 0, for all s ϵ S, a ϵ A(s)
    Initialize S, A
    Repeat (for each step of episode):
        Take action A, observe R, S'
        Choose A' from S' using policy derived from Q (e.g., ε-greedy)
        δ ← R + γ Q(S',A') - Q(S,A)
        E(S,A) ← E(S,A) + 1
        For all s ϵ S, a ϵ A(s):
            Q(s,a) ← Q(s,a) + α δ E(s,a)
            E(s,a) ← γ λ E(s,a)
        S ← S'; A ← A'
    until S is terminal
```

## Q-learning (off-policy TD control)
<!-- https://www.davidsilver.uk/wp-content/uploads/2020/03/control.pdf slide 38 -->
```
Initialize Q(s,a), for all s ϵ S, a ϵ A(s), arbitrarily, and Q(terminal-state, ·) = 0
Repeat (for each episode):
    Initialize S
    Repeat (for each step of episode):
        Choose A from S using policy derived from Q (e.g., ε-greedy)
        Take action A, observe R, S'
        Q(S,A) ← Q(S,A) + α [R + γ max_a{Q(S',a)} - Q(S,A)]
        S ← S';
    until S is terminal
```

<!-- Feel free to add more items in here: -->
# Unity ML-Agents ([Release 10](https://github.com/Unity-Technologies/ml-agents/releases/tag/release_10))

* [Installation](https://github.com/Unity-Technologies/ml-agents/blob/release_10/docs/Installation.md) & [Using an Environment Executable](https://github.com/Unity-Technologies/ml-agents/blob/release_10/docs/Learning-Environment-Executable.md)

* [Toolkit Overview](https://github.com/Unity-Technologies/ml-agents/blob/release_10/docs/ML-Agents-Overview.md) & [Example Learning Environments](https://github.com/Unity-Technologies/ml-agents/blob/release_10/docs/Learning-Environment-Examples.md)

* [Python Low Level API (`mlagents_envs`)](https://github.com/Unity-Technologies/ml-agents/blob/release_10/docs/Python-API.md) & [Gym Wrapper (`gym-unity`)](https://github.com/Unity-Technologies/ml-agents/blob/release_10/gym-unity/README.md)

* [Agents](https://github.com/Unity-Technologies/ml-agents/blob/release_10/docs/Learning-Environment-Design-Agents.md)
  * 🙈 [Observations](https://github.com/Unity-Technologies/ml-agents/blob/release_10/docs/Learning-Environment-Design-Agents.md#observations-and-sensors)
  * 🎭 [Actions](https://github.com/Unity-Technologies/ml-agents/blob/release_10/docs/Learning-Environment-Design-Agents.md#actions)
  * 💰 [Rewards](https://github.com/Unity-Technologies/ml-agents/blob/release_10/docs/Learning-Environment-Design-Agents.md#rewards)
  * 🤖 [Properties](https://github.com/Unity-Technologies/ml-agents/blob/release_10/docs/Learning-Environment-Design-Agents.md#agent-properties)
