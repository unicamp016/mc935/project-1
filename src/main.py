"""Agent training module."""
import argparse

from src.consts import Environment
from src.logger import log_info
from src.utils import unity_env


def main():
    """Train an agent."""
    with unity_env(Environment.TENNIS, no_graphics=True) as env:
        log_info(list(env.behavior_specs.items()))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    args = parser.parse_args()
    main(**vars(args))
