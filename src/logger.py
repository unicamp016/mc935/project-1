"""`print`-like functions to output colored text to the console."""
from enum import Enum
from os import getenv


class Color(Enum):
    RESET = '\033[0m'
    GREY = '\033[90m'  # debug
    RED = '\033[91m'  # error
    GREEN = '\033[92m'  # default
    YELLOW = '\033[93m'  # info
    BLUE = '\033[94m'
    MAGENTA = '\033[95m'  # warning
    CYAN = '\033[96m'
    WHITE = '\033[97m'


def _log_colored(color: Color, *args, **kwds):
    print(color.value, end="")
    print(*args, Color.RESET.value, **kwds)


def log(*args, **kwds):
    if not getenv("NO_LOG"):
        _log_colored(Color.GREEN, "[LOG]", *args, **kwds)


def log_debug(*args, **kwds):
    if not getenv("NO_DEBUG"):
        _log_colored(Color.GREY, "[DEBUG]", *args, **kwds)


def log_warning(*args, **kwds):
    if not getenv("NO_WARNING"):
        _log_colored(Color.MAGENTA, "[WARNING]", *args, **kwds)


def log_error(*args, **kwds):
    if not getenv("NO_ERROR"):
        _log_colored(Color.RED, "[ERROR]", *args, **kwds)


def log_info(*args, **kwds):
    if not getenv("NO_INFO"):
        _log_colored(Color.YELLOW, "[INFO]", *args, **kwds)


if __name__ == "__main__":
    # NOTE the actual value doesn't matter, only that it's not None
    print(f"{getenv('NO_LOG')=}")
    print(f"{getenv('NO_DEBUG')=}")
    print(f"{getenv('NO_WARNING')=}")
    print(f"{getenv('NO_ERROR')=}")
    print(f"{getenv('NO_INFO')=}")
