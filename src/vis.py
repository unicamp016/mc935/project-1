"""Visualizations."""
import os

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from celluloid import Camera
from matplotlib.patches import Circle
from mpl_toolkits.axes_grid1 import make_axes_locatable
from tqdm import tqdm

from src.consts import (BASIC_ACTIONS, BIN_DIR, TENNIS_OBS_NAMES,
                        TENNIS_PLAYER1, TENNIS_PLAYER2, Environment)
from src.utils import cont_to_disc, obs_bins, unity_env

action_to_arrow = {
    1: {
        'x': 0.25,
        'y': 0.5,
        'dx': -0.5,
        'dy': 0.0
    },
    2: {
        'x': -0.25,
        'y': 0.5,
        'dx': 0.5,
        'dy': 0.0
    },
}


def save_fig(fig, save_path=None):
    if save_path is None:
        fig.waitforbuttonpress()
    else:
        full_path = os.path.join(BIN_DIR, save_path)
        os.makedirs(os.path.dirname(full_path), exist_ok=True)
        fig.savefig(full_path)


def save_np(array, save_path):
    full_path = os.path.join(BIN_DIR, save_path)
    os.makedirs(os.path.dirname(full_path), exist_ok=True)
    np.save(full_path, array)


def plot_returns(returns):
    fig, ax = plt.subplots()
    ax.set_xlabel("Episódio")
    ax.set_ylabel("$G$")
    xs = np.arange(1, len(returns) + 1)
    ax.scatter(xs, returns, alpha=0.8)
    # for x, y in zip(xs, returns):
    # ax.annotate(f"{y:.2f}", (x + 0.1, y + 0.01))
    fig.waitforbuttonpress()
    plt.close(fig)


def plot_steps(steps, save_path=None):
    fig, ax = plt.subplots()
    ax.set_xlabel("Episódio")
    ax.set_ylabel("Passos")
    ax.plot(steps)
    save_fig(fig, os.path.join(save_path, "steps.png"))
    save_np(steps, os.path.join(save_path, "steps.npy"))


def vis_Qs(Qs, vis_freq, fps=2):
    episode = vis_freq
    fig, ax = plt.subplots()
    for Q in Qs:
        ax.set_title(f"Episode {episode}")
        ax.set_xlim(-0.5, len(Qs[0]) - 0.5)
        ax.set_yticks([])
        ax.set_xticks(np.arange(0, len(Qs[0]), 1))
        V_a = np.argmax(Q, axis=1)
        V = np.max(Q, axis=1, keepdims=True).T
        ax.imshow(V, cmap="inferno", extent=[-0.5, len(Q) - 0.5, 0, 1])
        for (i, j), _ in np.ndenumerate(V):
            action = V_a[j]
            if j in (7, 17):
                circle = Circle((j, 0.5), radius=j / 50, color="g", alpha=0.7)
                ax.add_patch(circle)
            elif action == 0:
                circle = Circle((j, 0.5), radius=0.1, color="r", alpha=0.7)
                ax.add_patch(circle)
            else:
                x, y, dx, dy = action_to_arrow[action].values()
                ax.arrow(x=j + x,
                         y=i + y,
                         dx=dx,
                         dy=dy,
                         length_includes_head=True,
                         head_width=0.1,
                         head_length=0.1,
                         color='r')
        plt.show(block=False)
        plt.pause(1 / fps)
        ax.clear()
        episode += vis_freq
    plt.close(fig)


def vis_Ns(Ns, vis_freq, fps=2):
    episode = vis_freq
    fig, ax = plt.subplots()
    for N in Ns:
        ax.set_title(f"Episódio {episode}")
        ax.imshow(N.T,
                  origin="lower",
                  cmap="magma",
                  extent=[-0.5, len(N) - 0.5, -0.5, 2.5])
        ax.set_xticks(np.arange(0, len(N)))
        ax.set_yticks(np.arange(0, len(N.T)))
        ax.set_yticklabels(BASIC_ACTIONS.values(), rotation=20)

        # Draw the number of times each (s,a) was visited
        black_label_thresh = np.max(N) * 0.8
        for (j, i), label in np.ndenumerate(N.T):
            color = "black" if label > black_label_thresh else "white"
            ax.text(i, j, label, ha="center", va="center", color=color)
        fig.tight_layout()
        plt.show(block=False)
        plt.pause(1 / fps)
        ax.clear()
        episode += vis_freq
    plt.close(fig)


def visualize_continuous_obs(steps=1000, save_path=None):
    """Plot a histogram for each feature in the observation space.

    Runs the environment for `steps` and accumulates the observations given
    a random action for each step. Then, a histogram for each feature
    in the observation space is plotted.

    Args:
        steps: Number of steps to run the simulation environment for.
        save_path: The filename relative to `BIN_DIR` to save the matplotlib
          Figure relative to the default `BIN_DIR`. If None, the plot is only
          shown.

    Returns:
        An n x 9 numpy array all observations stacked along the first dimension.
    """
    all_obs = []
    with unity_env(Environment.TENNIS, no_graphics=True) as env:
        behavior_spec, action_spec = env.behavior_specs[TENNIS_PLAYER1]
        # Divide by 3 since we always retrieve the last 3 observations.
        obs_size = behavior_spec[0][0] // 3
        for _ in tqdm(range(steps)):
            env.step()
            steps1 = env.get_steps(TENNIS_PLAYER1)
            steps2 = env.get_steps(TENNIS_PLAYER2)
            dec_obs1, term_obs1, dec_obs2, term_obs2 = tuple(
                [dstep.obs[0] for dstep in steps1 + steps2])

            # Get all observations from the last step
            obs = np.concatenate([dec_obs1.T, term_obs1.T, dec_obs2.T, term_obs2.T],
                                 axis=1)
            latest_obs = obs[-obs_size:, :]
            all_obs.append(latest_obs)

            # Set random actions for each player that requested a decision
            env.set_actions(TENNIS_PLAYER1, action_spec.random_action(dec_obs1.shape[0]))
            env.set_actions(TENNIS_PLAYER2, action_spec.random_action(dec_obs2.shape[0]))
    all_obs = np.concatenate(all_obs, axis=1)
    rows, cols = (3, 3)
    fig, axes = plt.subplots(nrows=rows, ncols=cols, figsize=(11, 11))
    cmap = matplotlib.cm.get_cmap("magma", 2 * rows * cols)
    for axid in range(rows * cols):
        ax = axes[axid // cols][axid % cols]
        ax.hist(all_obs[axid, :], bins=15, color=cmap.colors[axid])
        ax.set_title(TENNIS_OBS_NAMES[axid])
    fig.tight_layout(pad=1.5)
    if save_path is None:
        fig.waitforbuttonpress()
    else:
        fig.savefig(os.path.join(BIN_DIR, save_path))
    return all_obs


def visualize_discrete_obs(disc_obs, save_path=None):
    # pylint: disable=redefined-outer-name
    """Visualize the discrete observations space.

    Plot an image with one row for each feature and one column for each discrete
    bin used to discretize the continuous space. Each pixel in the image shows
    the number of observations that fall into that bin.

    Args:
        disc_obs: An m x n array of discrete observations, where m is the number
          of features and n is the number of observations.
        save_path: The filename relative to `BIN_DIR` to save the matplotlib
          Figure relative to the default `BIN_DIR`. If None, the plot is only
          shown.
    """
    feat_count = np.array(list(map(np.bincount, disc_obs)))
    fig, ax = plt.subplots(figsize=(10, 6))
    img = ax.imshow(feat_count, cmap="magma")

    # Place colorbar to the right of the image.
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.15)
    fig.colorbar(img, cax=cax)

    # Draw the bin count for each feature.
    black_label_thresh = np.max(feat_count) * 0.9
    for (j, i), label in np.ndenumerate(feat_count):
        color = "black" if label > black_label_thresh else "white"
        ax.text(i, j, label, ha="center", va="center", color=color)
    fig.tight_layout()
    if save_path is None:
        fig.waitforbuttonpress()
    else:
        fig.savefig(os.path.join(BIN_DIR, save_path))


if __name__ == "__main__":
    cont_obs = visualize_continuous_obs(200, "histogram.pdf")
    bins = obs_bins(cont_obs, 15, "bins.npy")
    disc_obs = cont_to_disc(cont_obs, bins)
    visualize_discrete_obs(disc_obs, "disc_space.pdf")
