"""Constants."""
from __future__ import annotations

import enum
import os
from collections import OrderedDict
from os.path import abspath, dirname
from typing import OrderedDict

PROJECT_DIR = dirname(dirname(abspath(__file__)))  # ./src/../
BIN_DIR = os.path.join(PROJECT_DIR, "bin")
os.makedirs(BIN_DIR, exist_ok=True)

IN_WIN32 = os.name == "nt"
BASE_ENV_PATH = os.path.join(PROJECT_DIR, "ml-agents", "Project", "Environments")


class Method(enum.Enum):
    """Available reinforcement learning methods."""

    MONTE_CARLO = "Monte Carlo"
    SARSA_LAMBDA = "SARSA(λ)"
    Q_LEARNING = "Q-learning"


class Environment(enum.Enum):
    """Available Unity environments."""

    BASIC = "Basic"
    BALL3D = "3DBall"
    TENNIS = "Tennis"

    @staticmethod
    def path_to(env: Environment) -> str:
        """Returns the full path to a Unity environment executable."""
        env_name = env.value if IN_WIN32 else env.value + ".x86_64"
        return os.path.join(BASE_ENV_PATH, env_name)


# yapf: disable #########################################################################

# Basic environment
BASIC_PLAYER = "Basic?team=0"
BASIC_OBS_NAMES = [
    # ml-agents/Project/Assets/ML-Agents/Examples/Basic/Scripts/BasicController.cs
    # NOTE the observation is a one-hot encoded vector with 1 for the current cube
    # position and 0 elsewhere (the small goal is at 7 and the large one is at 17)
    "cube_pos_1hot"  # 20 possible states (the agent/player starts at position 10)
]
BASIC_ACTIONS = OrderedDict({
    0: "stand",
    1: "left ",
    2: "right",
})

# 3DBall environment
BALL3D_PLAYER = "Basic?team=0"
BALL3D_OBS_NAMES = [
    # ml-agents/Project/Assets/ML-Agents/Examples/3DBall/Scripts/Ball3DAgent.cs
    "cube_rot_z", "cube_rot_x", "ball_pos", "ball_vel"
]

# Tennis environment
TENNIS_PLAYER1 = "Tennis?team=0"
TENNIS_PLAYER2 = "Tennis?team=1"
TENNIS_OBS_NAMES = [
    # ml-agents/Project/Assets/ML-Agents/Examples/Tennis/Scripts/TennisAgent.cs
    "rack_pos_x", "rack_pos_y", "rack_vel_x", "rack_vel_y",
    "ball_pos_x", "ball_pos_y", "ball_vel_x", "ball_vel_y",
    "rack_rot_z"
]

# yapf: enable ##########################################################################

if __name__ == "__main__":
    from src.logger import log
    log(f"{IN_WIN32=}")
    log(f"{PROJECT_DIR=}")
    log(f"{BASE_ENV_PATH=}")
