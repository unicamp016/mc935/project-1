"""SARSA(λ)."""

from typing import Any

import numpy as np
from mlagents_envs.environment import UnityEnvironment
from src.basic.agent import Agent, log_experience, unity_to_action
from src.basic.config import Config
from src.consts import BASIC_ACTIONS
from src.logger import log, log_debug, log_error, log_info, log_warning


class SarsaLambda(Agent):
    """SARSA(λ) learning agent."""

    def __init__(self, env: UnityEnvironment, cfg: Config) -> None:
        super().__init__(env, cfg)
        self.id = f"sarsalambda-{self.id}"
        self._Q = np.zeros((self._num_states, self._num_actions), dtype=np.float64)
        self._E = np.zeros((self._num_states, self._num_actions),
                           dtype=np.float64)  # state-action eligibility trace
        self._lambd = cfg.lambd

    def TD_error(self, state: int, action: int, reward: float, next_Q: float) -> float:
        """Return the TD-error `δ_t = (TD-target - Q(S_t, A_t))`.
        Where TD-target is `R_{t+1} + γ * Q(S_{t+1}, A_{t+1})`.
        """
        target = reward + self._gamma * next_Q
        error = target - self._Q[state, action]
        log(f"TD-target={target:.4f}, TD-error={error:.4f}")
        return error

    def _act(self, state: int, training: bool) -> int:
        if training and np.random.rand() < self.epsilon(state):
            return unity_to_action(self.behavior_spec.action_spec.random_action(1))
        max_Q = np.max(self._Q[state])  # greedy action choice
        return np.random.choice(np.where(self._Q[state] == max_Q)[0])

    def _update(self, state: int, action: int, reward: float, next_state: int,
                done: bool) -> None:
        log_experience(state, action, reward, next_state)

        # Update `_N`
        self._N[state, action] += 1

        alpha = self.alpha(state, action)

        next_action = self._act(next_state, training=True)  # NOTE on-policy
        next_Q = self._Q[next_state, next_action] if not done else 0.0
        error = self.TD_error(state, action, reward, next_Q)

        log(f"a' = {BASIC_ACTIONS[next_action]}, Q(s',a') = {next_Q:.4f}")
        log(f"α = {alpha:.4f}, δ = {error:.4f}")

        # Update `_E` and `_Q` according to SARSA(λ)
        self._E[state, action] += 1  # NOTE accumulating traces
        for s in range(self._num_states):
            for a in range(self._num_actions):
                self._Q[s, a] += alpha * error * self._E[s, a]
                self._E[s, a] *= self._gamma * self._lambd  # decay

    def _at_episode_end(self, episode: int, max_episodes: int, training: bool) -> None:
        if training:
            if episode % self._vis_freq == 0:
                self.Qs.append(self._Q.copy())
                self.Ns.append(self._N.copy())
            log_on_last_episode = (lambda *args, **kwds: log_debug(*args, **kwds)
                                   if episode == max_episodes else None)
            log_on_last_episode(f"E = \n{self._E}, ", end="")
            log_debug(f"Σ(E) = {np.sum(self._E):.4f}")
            log_on_last_episode(f"N = \n{self._N}, ", end="")
            log_debug(f"Σ(N) = {np.sum(self._N)}")
            log_on_last_episode(f"Q = \n{self._Q}")
            log_debug(f"Q(s=10) = {self._Q[10]}  (argmax = {np.argmax(self._Q[10])})")
            log_debug(f"argmax(Q) = \n{np.argmax(self._Q, axis=1)}"
                      "\n               ^     ^             ^"
                      "\n               7     10            17")

    def reset(self) -> int:
        # Reinitialize the eligibility traces to 0.0
        self._E = np.zeros((20, 3), dtype=np.float64)
        return super().reset()

    def run(self, cfg: Config, training: bool) -> Any:
        assert not training or np.sum(self._E) == 0.0
        assert not training or np.sum(self._Q) == 0.0
        return super().run(cfg, training=training)


def feature(state: int, action: int) -> np.ndarray:
    """Compute a feature from a state-action pair."""
    # FIXME move to Agent and use self._num_states and self._num_actions
    one_hot_state = np.zeros((20,), dtype=np.int32)
    one_hot_state[state] = 1.0
    one_hot_action = np.zeros((3,), dtype=np.int32)
    one_hot_action[action] = 1.0
    return np.concatenate([one_hot_state, one_hot_action, [1]])


class SarsaLambdaLinear(Agent):
    """SARSA(λ) learning agent with linear function approximator."""

    def __init__(self, env: UnityEnvironment, cfg: Config) -> None:
        self._w = np.random.normal(size=24)  # FIXME see reset()
        self._E = np.zeros(self._w.shape, dtype=np.float64)  # eligibility trace
        super().__init__(env, cfg)
        self._lambd = cfg.lambd
        self.id = f"sarsalambda-lfa-{self.id}"

    def TD_error(self, state: int, action: int, reward: float, next_Q: float) -> float:
        """Return the TD-error `δ_t = (TD-target - Q(S_t, A_t))`.
        Where TD-target is `R_{t+1} + γ * Q(S_{t+1}, A_{t+1})`.
        """
        target = reward + self._gamma * next_Q
        error = target - np.dot(feature(state, action), self._w)  # linear approximation
        log(f"TD-target={target:.4f}, TD-error={error:.4f}")
        return error

    def _act(self, state: int, training: bool) -> int:
        if training and np.random.rand() < self.epsilon(state):
            return unity_to_action(self.behavior_spec.action_spec.random_action(1))
        features = np.array([feature(state, action) for action in range(3)])
        Qs = np.dot(features, self._w)  # linear approximation
        max_Q = np.max(Qs)  # greedy action choice
        return np.random.choice(np.where(Qs == max_Q)[0])

    def _update(self, state: int, action: int, reward: float, next_state: int,
                done: bool) -> None:
        log_experience(state, action, reward, next_state)

        # Update `_N`
        self._N[state, action] += 1

        alpha = self.alpha(state, action)

        next_action = self._act(next_state, training=True)  # NOTE on-policy
        next_Q = (
            np.dot(feature(next_state, next_action), self._w)  # linear approximation
            if not done else 0.0)
        xxx_ones = [i for i, f in enumerate(feature(next_state, next_action)) if f == 1]
        error = self.TD_error(state, action, reward, next_Q)

        log_debug(f"weights = {self._w}")
        log_debug(f"feature(s,a)   = {feature(state, action)}")
        log_debug(f"feature(s',a') = {feature(next_state, next_action)}")

        log(f"a' = {BASIC_ACTIONS[next_action]}, Q(s',a') = {next_Q:.4f}")
        log(f"α = {alpha:.4f}, δ = {error:.4f}")

        # Update `_E` and `_w` according to SARSA(λ)
        # FIXME this isn't the right way to index it...
        # self._E[feature(state, action)] += 1  # NOTE accumulating traces
        self._E[xxx_ones] += 1  # NOTE accumulating traces
        self._w += alpha * error * self._E
        self._E *= self._gamma * self._lambd  # decay

    def _at_episode_end(self, episode: int, max_episodes: int, training: bool) -> None:
        if training:
            if episode % self._vis_freq == 0:
                Q = np.zeros(shape=(self._num_states, self._num_actions),
                             dtype=np.float32)
                for state, action in np.ndindex(Q.shape):
                    Q[state, action] = np.dot(feature(state, action), self._w)
                self.Qs.append(Q)
                self.Ns.append(self._N.copy())
            last_episode = episode == max_episodes
            log_on_last_episode = (lambda *args, **kwds: log_debug(*args, **kwds)
                                   if last_episode else None)
            log_on_last_episode(f"E = \n{self._E}, ", end="")
            log_debug(f"Σ(E) = {np.sum(self._E):.4f}")
            log_on_last_episode(f"N = \n{self._N}, ", end="")
            log_debug(f"Σ(N) = {np.sum(self._N)}")
            log_debug(f"weights = {self._w}")
            if last_episode:
                for state in range(20):
                    features = np.array([feature(10, action) for action in range(3)])
                    log_on_last_episode(
                        f"state {state} -> {np.argmax(np.dot(features, self._w))}")

    def reset(self) -> int:
        # Reinitialize the eligibility traces to 0.0
        self._E = np.zeros(self._w.shape, dtype=np.float64)
        return super().reset()

    def run(self, cfg: Config, training: bool) -> Any:
        assert not training or np.sum(self._E) == 0.0
        return super().run(cfg, training=training)
