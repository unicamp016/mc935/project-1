from typing import Optional

import numpy as np
from mlagents_envs.environment import UnityEnvironment
from src.basic.agent import Agent, log_experience, unity_to_action
from src.basic.config import Config
from src.logger import log


class QLearning(Agent):
    """Q-Learning."""

    def __init__(self, env: UnityEnvironment, cfg: Config) -> None:
        super().__init__(env, cfg)
        self.id = f"qlearn-{self.id}"
        self._Q = np.zeros((self._num_states, self._num_actions), dtype=np.float64)

    def TD_error(self, state: int, action: int, reward: float, next_state: int,
                 done: bool) -> float:
        """Return the TD-error `(δ_t - Q(S_t, A_t))`, where `δ_t` is the TD-target."""
        next_Q = self.Q_value(next_state) if not done else 0.0
        target = reward + self._gamma * next_Q
        return target - self.Q_value(state, action)

    def _act(self, state: int, training: bool) -> int:
        """Determine the next action according to ε-greedy exloration strategy
        if `training is True`. Otherwise (i.e. we are evaluating), act greedy.
        """
        if training and np.random.rand() < self.epsilon(state):
            return unity_to_action(self.behavior_spec.action_spec.random_action(1))
        max_Q = self.Q_value(state)  # greedy action choice
        return np.random.choice(np.where(self._Q[state] == max_Q)[0])

    def Q_value(self, state: int, action: Optional[int] = None) -> float:
        """Return the Q-value for the given state-action pair."""
        if action is None:
            return np.max(self._Q[state])
        return self._Q[state, action]

    def _at_episode_end(self, episode: int, max_episodes: int, training: bool) -> None:
        """Add visualizations."""
        if training and episode % self._vis_freq == 0:
            self.Qs.append(self._Q.copy())
            self.Ns.append(self._N.copy())

    def _update(self, state: int, action: int, reward: float, next_state: int,
                done: bool) -> None:
        """Update the values of `Q` and `N`."""
        self._N[state, action] += 1
        log_experience(state, action, reward, next_state)
        delta = self.TD_error(state, action, reward, next_state, done)
        alpha = self.alpha(state, action)
        log(f"α={alpha}, TD-error={delta:.4f}")
        self._Q[state, action] += alpha * delta


class QLearningLinear(Agent):
    """Q-Learning with linear approximation."""

    def __init__(self, env: UnityEnvironment, cfg: Config) -> None:
        super().__init__(env, cfg)
        self.id = f"qlearn-lfa-{self.id}"
        self._w = np.random.normal(size=self._num_states + self._num_actions + 1)

    def TD_error(self, state: int, action: int, reward: float, next_state: int,
                 done: bool) -> float:
        """Return the TD-error `(δ_t - Q(S_t, A_t))`, where `δ_t` is the TD-target."""
        next_Q = np.max(self.Q_value(next_state)) if not done else 0.0
        target = reward + self._gamma * next_Q
        return target - self.Q_value(state, action)

    def _act(self, state: int, training: bool) -> int:
        """Determine the next action according to ε-greedy exloration strategy
        if `training is True`. Otherwise (i.e. we are evaluating), act greedy.
        """
        if training and np.random.rand() < self.epsilon(state):
            return unity_to_action(self.behavior_spec.action_spec.random_action(1))
        Qs = self.Q_value(state)
        return np.argmax(Qs)  # greedy action choice

    def feature(self, state: int, action: int) -> np.ndarray:
        """Compute a feature from a state and action pair."""
        one_hot_state = np.zeros(self._num_states)
        one_hot_state[state] = 1
        one_hot_action = np.zeros(self._num_actions)
        one_hot_action[action] = 1
        return np.concatenate([one_hot_state, one_hot_action, [1]])

    def Q_value(self, state: int, action: Optional[int] = None) -> float:
        """Return the Q-value for the given state-action pair. If an action is provided,
        a single Q-value for the state-action pair is returned. Otherwise, a vector of
        all the Q-values for each action in a given state is returned."""
        if action is None:
            features = []
            for act in range(self._num_actions):
                features.append(self.feature(state, act))
            features = np.array(features)
            return np.dot(features, self._w)
        return np.dot(self.feature(state, action), self._w)

    def _at_episode_end(self, episode: int, max_episodes: int, training: bool) -> None:
        """Don't do anything for now."""
        if training and episode % self._vis_freq == 0:
            Q = np.zeros(shape=(self._num_states, self._num_actions), dtype=np.float32)
            for state, action in np.ndindex(Q.shape):
                Q[state, action] = self.Q_value(state, action)
            self.Qs.append(Q)
            self.Ns.append(self._N.copy())

    def _update(self, state: int, action: int, reward: float, next_state: int,
                done: bool) -> None:
        """Update the values of `w` and `N`."""
        self._N[state, action] += 1
        log_experience(state, action, reward, next_state)
        delta = self.TD_error(state, action, reward, next_state, done)
        alpha = self.alpha(state, action)
        log(f"α={alpha}, TD-error={delta:.4f}")
        self._w += alpha * delta * self.feature(state, action)
