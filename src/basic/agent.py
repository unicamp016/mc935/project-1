"""Base class for learning agents."""
from typing import Any, Optional, Tuple

import numpy as np
from mlagents_envs.base_env import ActionTuple
from mlagents_envs.environment import UnityEnvironment
from src.basic.config import Config
from src.consts import BASIC_ACTIONS
from src.logger import log, log_info, log_warning


def log_experience(state: int, action: int, reward: float, next_state: int) -> None:
    """Log a `(s, a, r, s')` tuple to console."""
    log(f"(s, a, r, s') = ({state}, {BASIC_ACTIONS[action]}, {reward:.2f}, {next_state})")


def unity_to_state(obs: np.ndarray) -> int:
    """Return the index of the state given a `UnityEnvironment` observation."""
    state = np.argmax(obs)
    return state


def unity_to_action(action_tuple: ActionTuple) -> int:
    """Return the index of the action given a `UnityEnvironment` ActionTuple."""
    assert action_tuple.discrete.shape == (1, 1)
    action = action_tuple.discrete[0, 0]
    return action


def action_to_unity(action: int) -> ActionTuple:
    """Builds an `ActionTuple` for a `UnityEnvironment` from the given action index."""
    action_tuple = ActionTuple()
    action_tuple.add_discrete(np.array(action).reshape((1, 1)))
    return action_tuple


class Agent:
    """Base class for learning agents on the Basic ML-Agents environment."""

    def __init__(self, env: UnityEnvironment, cfg: Config) -> None:
        self.id = f"{cfg.n0}-{cfg.gamma}-{cfg.episodes}"

        self.env = env
        self.env.reset()
        if not self.env.behavior_specs:
            self.env.step()

        assert len(self.env.behavior_specs) == 1
        self.brain = list(self.env.behavior_specs)[0]
        self.behavior_spec = self.env.behavior_specs[self.brain]
        assert self.behavior_spec.action_spec.is_discrete()
        self.reset()

        log(f"observation_shapes = {self.behavior_spec.observation_shapes}")
        log(f"action_spec = {self.behavior_spec.action_spec}")
        log(f"discrete_size = {self.behavior_spec.action_spec.discrete_size}")

        # Basic has 1 discrete action branch with 3 actions (do nothing,
        # move left, move right), and one variable corresponding to the
        # current state stored as a one-hot encoded 1 x 20 numpy array.
        assert self.behavior_spec.action_spec.discrete_branches == (3,)
        assert self.behavior_spec.action_spec.discrete_size == 1
        assert self.behavior_spec.observation_shapes == [(20,)]

        self._num_states = 20
        self._num_actions = 3
        self._N = np.zeros((self._num_states, self._num_actions),
                           dtype=np.int32)  # state-action visits counter
        self._N0 = cfg.n0
        self._gamma = cfg.gamma
        self._vis_freq = cfg.vis_freq
        self.Qs = []
        self.Ns = []

    def N(self, state: int, action: Optional[int] = None) -> int:
        """Return the number of times that:
        - State `s` has been visited, if `a is None`
        - Action `a` has been selected from state `s`, if `a is not None`
        """
        if action is None:
            return np.sum(self._N[state])
        return self._N[state, action]

    def alpha(self, state: int, action: int) -> float:
        """Return the (time-varying) learning rate `α = 1 / N(s,a)`."""
        return 1 / self.N(state, action)

    def epsilon(self, state: int) -> float:
        """Return the (time-varying) ε-greedy step size `ε = N0 / (N0 + N(s))`."""
        return self._N0 / (self._N0 + self.N(state))

    def _act(self, state: int, training: bool) -> int:
        """Determine the next action according to ε-greedy exloration strategy
        if `training is True`. Otherwise (i.e. we are evaluating), act greedy.
        """
        raise NotImplementedError

    def _update(self, state: int, action: int, reward: float, next_state: int,
                done: bool) -> None:
        """Update called after every state transition."""
        raise NotImplementedError

    def _at_episode_end(self, episode: int, max_episodes: int, training: bool) -> None:
        """Code to be called at the end of each episode."""
        raise NotImplementedError

    def reset(self) -> int:
        """Reset the environment and return an initial state."""
        self.env.reset()
        decision_steps, _ = self.env.get_steps(self.brain)
        assert len(decision_steps) == 1
        return unity_to_state(decision_steps.obs[0][0])

    def step(self, action: int) -> Tuple[float, int, bool]:
        """Run one timestep of the environment, returning `(reward, state, done)`."""
        action_tuple = action_to_unity(action)
        self.env.set_actions(self.brain, action_tuple)
        self.env.step()
        decision_steps, terminal_steps = self.env.get_steps(self.brain)
        assert bool(len(terminal_steps) == 1) != bool(len(decision_steps) == 1)
        if len(terminal_steps) == 0:
            reward, obs = decision_steps.reward[0], decision_steps.obs[0][0]
            return reward, unity_to_state(obs), False
        else:
            reward, obs = terminal_steps.reward[0], terminal_steps.obs[0][0]
            return reward, unity_to_state(obs), True

    def train(self, cfg: Config) -> Any:
        return self.run(cfg, training=True)

    def evaluate(self, cfg: Config) -> Any:
        episode_reward, episode_step = self.run(cfg, training=False)
        log_info(
            f"Avg. return over {cfg.episodes} episodes: {np.mean(episode_reward):.4f}")
        return episode_reward, episode_step

    def run(self, cfg: Config, training: bool = True) -> Any:
        episodes_reward = []
        episodes_step = []
        episodes_val_return = []

        assert not training or np.sum(self._N) == 0

        for episode in range(1, cfg.episodes + 1):
            if cfg.max_steps is not None:
                log_info(f"=== Episode {episode} / {cfg.episodes}")
            else:
                log_info(f"=== Episode {episode} / {cfg.episodes}"
                         f"  ({cfg.max_steps} max steps)")

            step = 0
            done = False
            episode_reward = 0.0
            state = self.reset()

            while not done:
                if cfg.max_steps is not None and step >= cfg.max_steps:
                    break
                log_info(f"=== step {step}")

                action = self._act(state, training)
                reward, next_state, done = self.step(action)

                if training:
                    self._update(state, action, reward, next_state, done)
                log_experience(state, action, reward, next_state)

                step += 1
                episode_reward += reward
                state = next_state
            if training:
                log_warning(f"Done! ({step} steps)")
                val_return, _, _ = self.run(Config(episodes=1, max_steps=30),
                                            training=False)
                episodes_val_return.extend(val_return)
            episodes_reward.append(episode_reward)
            episodes_step.append(step)
            self._at_episode_end(episode, cfg.episodes, training)
        return episodes_reward, episodes_step, episodes_val_return
