import argparse

import numpy as np
from src.basic.config import (MONTE_CARLO_CONFIG, MONTE_CARLO_LFA_CONFIG,
                              Q_LEARNING_CONFIG, Q_LEARNING_LFA_CONFIG,
                              SARSA_LAMBDA_CONFIG, SARSA_LAMBDA_LFA_CONFIG,
                              Config)
from src.basic.mc import MonteCarlo, MonteCarloLinear
from src.basic.qlearn import QLearning, QLearningLinear
from src.basic.sarsalambda import SarsaLambda, SarsaLambdaLinear
from src.consts import Environment, Method
from src.utils import unity_env
from src.vis import plot_returns, vis_Ns, vis_Qs

parser = argparse.ArgumentParser()
parser.add_argument("--env_name",
                    default=Environment.BASIC.name,
                    choices=[env.name for env in Environment],
                    help="Name of a Unity environment  (default: \"%(default)s\").")
parser.add_argument("--method",
                    default=Method.SARSA_LAMBDA.name,
                    choices=[method.name for method in Method],
                    help="Name of the learning method  (default: \"%(default)s\").")
parser.add_argument("--linear",
                    action="store_true",
                    help="Use a linear function approximator.")
args = parser.parse_args()

cfg = Config()
np.random.seed(cfg.seed)

with unity_env(args.env_name, no_graphics=True, seed=cfg.seed) as env:
    method = Method[args.method]
    if not args.linear:
        print(f"::: {method.value} :::")
        if method == Method.SARSA_LAMBDA:
            cfg = SARSA_LAMBDA_CONFIG
            trainer = SarsaLambda(env, cfg)
        elif method == Method.Q_LEARNING:
            cfg = Q_LEARNING_CONFIG
            trainer = QLearning(env, cfg)
        elif method == Method.MONTE_CARLO:
            cfg = MONTE_CARLO_CONFIG
            trainer = MonteCarlo(env, cfg)
        else:
            raise NotImplementedError
    else:
        print(f"::: {method.value} (with Linear Function Approximator) :::")
        if method == Method.SARSA_LAMBDA:
            cfg = SARSA_LAMBDA_LFA_CONFIG
            trainer = SarsaLambdaLinear(env, cfg)
        elif method == Method.Q_LEARNING:
            cfg = Q_LEARNING_LFA_CONFIG
            trainer = QLearningLinear(env, cfg)
        elif method == Method.MONTE_CARLO:
            cfg = MONTE_CARLO_LFA_CONFIG
            trainer = MonteCarloLinear(env, cfg)
        else:
            raise NotImplementedError

    print(f"\n{cfg}\n")
    _, _, val_returns = trainer.train(cfg)
    plot_returns(val_returns)
    vis_Qs(trainer.Qs, cfg.vis_freq)
    vis_Ns(trainer.Ns, cfg.vis_freq)
