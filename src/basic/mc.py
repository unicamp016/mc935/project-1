from itertools import accumulate
from typing import Optional

import numpy as np
from mlagents_envs.environment import UnityEnvironment
from src.basic.agent import Agent, log_experience, unity_to_action
from src.basic.config import Config
from src.logger import log


class Episode:
    """Keep track of experiences for an episode."""

    def __init__(self, gamma):
        self._gamma = gamma
        self.rewards = []
        self.actions = []
        self.states = []
        self.next_states = []
        self.G = []

    def __len__(self):
        return len(self.rewards)

    def __iter__(self):
        for t in range(len(self)):
            yield self.states[t], self.actions[t], self.G[t]

    def append(self, state: int, action: int, reward: float, next_state: int) -> None:
        self.rewards.append(reward)
        self.actions.append(action)
        self.states.append(state)
        self.next_states.append(next_state)
        self.G.append(reward)
        G_update = reward
        for i in reversed(range(len(self.G) - 1)):
            G_update = self._gamma * G_update
            self.G[i] += G_update

    def clear(self):
        self.rewards = []
        self.actions = []
        self.states = []
        self.next_states = []
        self.G = []


class MonteCarlo(Agent):
    """Monte Carlo Control."""

    def __init__(self, env: UnityEnvironment, cfg: Config) -> None:
        super().__init__(env, cfg)
        self.id = f"mc-{self.id}"
        self._Q = np.zeros((self._num_states, self._num_actions), dtype=np.float64)
        self._episode = Episode(self._gamma)

    def _act(self, state: int, training: bool) -> int:
        """Determine the next action according to ε-greedy exloration strategy
        if `training is True`. Otherwise (i.e. we are evaluating), act greedy.
        """
        if training and np.random.rand() < self.epsilon(state):
            return unity_to_action(self.behavior_spec.action_spec.random_action(1))
        max_Q = np.max(self._Q[state])  # greedy action choice
        return np.random.choice(np.where(self._Q[state] == max_Q)[0])

    def _at_episode_end(self, episode: int, max_episodes: int, training: bool) -> None:
        """Update the Q value table using the entire episode."""
        visited = set()
        for state, action, G in self._episode:
            if (state, action) not in visited:
                visited.add((state, action))
                self._N[state, action] += 1
                alpha = self.alpha(state, action)
                error = G - self._Q[state, action]
                self._Q[state, action] += alpha * error
        self._episode.clear()
        if training and episode % self._vis_freq == 0:
            self.Qs.append(self._Q.copy())
            self.Ns.append(self._N.copy())

    def _update(self, state: int, action: int, reward: float, next_state: int,
                done: bool) -> None:
        """Keep track of the experiences for each step."""
        log_experience(state, action, reward, next_state)
        self._episode.append(state, action, reward, next_state)


class MonteCarloLinear(Agent):
    """Monte Carlo Control with linear function approximation."""

    def __init__(self, env: UnityEnvironment, cfg: Config) -> None:
        super().__init__(env, cfg)
        self.id = f"mc-lfa-{self.id}"
        self._w = np.random.normal(size=self._num_states + self._num_actions + 1)
        self._episode = Episode(self._gamma)

    def feature(self, state: int, action: int) -> np.ndarray:
        """Compute a feature from a state and action pair."""
        one_hot_state = np.zeros(self._num_states)
        one_hot_state[state] = 1
        one_hot_action = np.zeros(self._num_actions)
        one_hot_action[action] = 1
        return np.concatenate([one_hot_state, one_hot_action, [1]])

    def Q_value(self, state: int, action: Optional[int] = None) -> float:
        """Return the Q-value for the given state-action pair. If an action is provided,
        a single Q-value for the state-action pair is returned. Otherwise, a vector of
        all the Q-values for each action in a given state is returned."""
        if action is None:
            features = []
            for act in range(self._num_actions):
                features.append(self.feature(state, act))
            features = np.array(features)
            return np.dot(features, self._w)
        return np.dot(self.feature(state, action), self._w)

    def _act(self, state: int, training: bool) -> int:
        """Determine the next action according to ε-greedy exloration strategy
        if `training is True`. Otherwise (i.e. we are evaluating), act greedy.
        """
        if training and np.random.rand() < self.epsilon(state):
            return unity_to_action(self.behavior_spec.action_spec.random_action(1))
        Qs = self.Q_value(state)
        return np.argmax(Qs)  # greedy action choice

    def _at_episode_end(self, episode: int, max_episodes: int, training: bool) -> None:
        """Update the weight vector from the episode's experiences."""
        visited = set()
        for state, action, G in self._episode:
            if (state, action) not in visited:
                visited.add((state, action))
                self._N[state, action] += 1
                alpha = self.alpha(state, action)
                delta = G - self.Q_value(state, action)
                self._w += alpha * delta * self.feature(state, action)
        self._episode.clear()
        if training and episode % self._vis_freq == 0:
            Q = np.zeros(shape=(self._num_states, self._num_actions), dtype=np.float32)
            for state, action in np.ndindex(Q.shape):
                Q[state, action] = self.Q_value(state, action)
            self.Qs.append(Q)
            self.Ns.append(self._N.copy())

    def _update(self, state: int, action: int, reward: float, next_state: int,
                done: bool) -> None:
        """Keep track of the experiences for each step."""
        log_experience(state, action, reward, next_state)
        self._episode.append(state, action, reward, next_state)
