"""Hyper-parameters."""

from typing import NamedTuple, Optional


class Config(NamedTuple):
    """Hyper-parameters.

    - Constant used to compute α and ε
    - Discount factor γ
    - Trace decay parameter λ (for SARSA(λ))
    - Number of episodes
    - Number of episodes between each visualization
    - Max number of steps per episode
    - Random seed
    """

    n0: int = 5000
    gamma: float = 0.95
    lambd: float = 0.60
    episodes: int = 300
    vis_freq: int = 2
    max_steps: Optional[int] = None
    seed: int = 123  # FIXME this is the seed that's used (see below)


# FIXME changing the seed in here won't have any effect (see main.py)
Q_LEARNING_CONFIG = Config(n0=5000, gamma=0.95, episodes=200, max_steps=50)
Q_LEARNING_LFA_CONFIG = Config(n0=5000, gamma=0.95, episodes=10, max_steps=50)
SARSA_LAMBDA_CONFIG = Config(n0=500, gamma=0.95, lambd=0.8, episodes=50)
SARSA_LAMBDA_LFA_CONFIG = Config(n0=500, gamma=0.95, lambd=0.8, episodes=10)
MONTE_CARLO_CONFIG = Config(n0=5000, gamma=0.95, episodes=20, max_steps=50)
MONTE_CARLO_LFA_CONFIG = Config(n0=5000, gamma=0.95, episodes=10, max_steps=50)
