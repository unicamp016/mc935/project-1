"""Utility functions."""
import os
from contextlib import contextmanager

import numpy as np
from mlagents_envs.environment import UnityEnvironment

from src.consts import BIN_DIR, Environment


@contextmanager
def unity_env(env_name, **kwargs):
    """Context manager for a UnityEnvironment.

    Args:
        env_name: An Environment enum type.
        kwargs: Keyword arguments used to create the UnityEnvironment.

    Yields:
        A reset UnityEnvironment ready for reading.
    """
    try:
        if isinstance(env_name, str):
            env_path = Environment.path_to(Environment[env_name])
        else:
            env_path = Environment.path_to(env_name)
    except KeyError as e:
        raise NotImplementedError(
            f"Requested environment '{env_name}' isn't available.") from e
    env = UnityEnvironment(env_path, **kwargs)
    env.reset()
    try:
        yield env
    finally:
        env.close()


def obs_bins(obs, num_bins, save_path=None):
    """Compute feature-wise `num_bins` bins for the given observations.

    Args:
        obs: An m x n array of observations, where m is the number of features
          and n is the number of observations.
        num_bins: The number of bins we wish create for each observation feature range.
        save_path: The filname relative to `BIN_DIR` to save the bins to.

    Returns:
        A m x b array with the bins for each feature in the observation space.
    """
    bins = np.linspace(np.min(obs, axis=1), np.max(obs, axis=1), num=num_bins + 1, axis=1)
    # Discard the last bin in order for the bins to play nicely with
    # np.digitize.  If we kept the last bin, there would always be a single
    # value mapped to it, which is the max value of the array. By sampling b +
    # 1 bins and discarding the last, we ensure that there are effectively b
    # uniform intervals in which the continuous data can be mapped to.
    bins = bins[:, :-1]
    if save_path is not None:
        np.save(os.path.join(BIN_DIR, save_path), bins)
    return bins


def cont_to_disc(cont_obs, bins):
    """Transform a set of continuous observations to discrete ones.

    Args:
        cont_obs: An m x n array of m-dimensional continuous observations.
        bins: An m x b array with b bins for each feature m.

    Returns:
        An m x n array with the stacked m-dimensional discrete observations.
        The discrete observations are in the range [0, b - 1].
    """
    disc_obs = np.array(
        list(
            map(lambda ob: np.digitize(ob[0], ob[1], right=False) - 1,
                zip(cont_obs, bins))))
    return disc_obs
