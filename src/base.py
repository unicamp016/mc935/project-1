"""Base classes and types."""

from collections import defaultdict
from typing import Any, DefaultDict, Dict, List, NamedTuple, Tuple, Union

import numpy as np
from mlagents_envs.base_env import (ActionSpec, BehaviorSpec, DecisionStep, DecisionSteps,
                                    TerminalSteps)
from mlagents_envs.environment import BaseEnv


class Experience(NamedTuple):
    """An experience contains the data of one agent transition:
       - Observation
       - Action
       - Reward
       - Next observation
       - Done flag

    Reference: https://colab.research.google.com/drive/1nkOztXzU91MHEbuQ1T9GnynYdL_LRsHG
    """
    obs: np.ndarray  # state: S_{t}
    action: np.ndarray  # action: A_{t}
    reward: float  # reward: R_{t+1}
    next_obs: np.ndarray  # state: S_{t+1}
    done: bool


# A Trajectory is an ordered sequence of experiences
Trajectory = List[Experience]

# A Buffer is an unordered list of experiences from multiple trajectories
Buffer = List[Experience]

# An action that can be taken by an agent
Action = Any


class Policy:
    """A policy is called on an agent's observations to choose and return the next
    actions to be played out in the environment by such agent.

    Reference: https://github.com/Unity-Technologies/ml-agents/blob/master/ml-agents/mlagents/trainers/policy/policy.py
    """

    def __init__(self,
                 name: str,
                 behavior_spec: BehaviorSpec,
                 seed: int = 1,
                 gamma: float = 0.95) -> None:
        self.name = name
        self.seed = seed
        self.gamma = gamma

        self.behavior_spec = behavior_spec
        self.act_size = (list(self.behavior_spec.action_spec.discrete_branches)
                         if self.behavior_spec.action_spec.is_discrete() else
                         [self.behavior_spec.action_spec.continuous_size])

    def __call__(self, decision_step: DecisionStep) -> Action:
        """Decides actions given observations information."""
        raise NotImplementedError

    # NOTE I added some method shortcuts below which might be useful,
    # but feel free to delete them if it's only cluttering this file.

    @property
    def observation_shapes(self) -> List[Tuple]:
        return self.behavior_spec.observation_shapes

    @property
    def action_spec(self) -> ActionSpec:
        return self.behavior_spec.action_spec

    @property
    def is_continuous(self) -> bool:
        return self.behavior_spec.action_spec.is_continuous()

    @property
    def is_discrete(self) -> bool:
        return self.behavior_spec.action_spec.is_discrete()


# TRAINER ###############################################################################

# Contains the data a batch of similar agents collected in a simulation step
EnvSteps = Union[DecisionSteps, TerminalSteps]


class Trainer:
    """Reference: https://colab.research.google.com/drive/1nkOztXzU91MHEbuQ1T9GnynYdL_LRsHG"""

    @staticmethod
    def generate_trajectories(env: BaseEnv, policy: Policy,
                              buffer_size: int) -> Tuple[Buffer, float]:
        """Returns a buffer of experiences obtained by running the Unity
        environment with the given policy, and the average cumulative reward
        the agent obtained.
        """
        env.reset()

        # FIXME this returns two behaviors for Tennis (one for each agent)
        # Read and store the first behavior name of the environment and its specs
        assert len(env.behavior_specs) == 1
        behavior_name, *_ = list(env.behavior_specs)
        _spec = env.behavior_specs[behavior_name]

        # Create mappings from the agent id
        agent_last_obs: Dict[int, np.ndarray] = {}
        agent_last_action: Dict[int, np.ndarray] = {}
        agent_cumulative_reward: DefaultDict[int, float] = defaultdict(lambda: 0.0)
        agent_trajectories: DefaultDict[int, Trajectory] = defaultdict(lambda: [])

        def experience_from(agent_id: int, steps: EnvSteps, done: bool) -> Experience:
            """Return a `(s, a, r, s', done)` tuple."""
            return Experience(obs=agent_last_obs[agent_id].copy(),
                              action=agent_last_action[agent_id].copy(),
                              reward=steps[agent_id].reward,
                              next_obs=steps[agent_id].obs[0],
                              done=done)

        buffer: Buffer = []
        cumulative_rewards: List[float] = []

        while len(buffer) < buffer_size:
            decision_steps, terminal_steps = env.get_steps(behavior_name)

            # For all agents with a terminal step:
            for id in terminal_steps:
                last_experience = experience_from(id,
                                                  terminal_steps,
                                                  done=not terminal_steps[id].interrupted)

                agent_last_obs.pop(id)  # clear its last observation and action
                agent_last_action.pop(id)  # (since the trajectory is over)

                cumulative_rewards.append(
                    agent_cumulative_reward.pop(id) + terminal_steps[id].reward)

                buffer.extend(agent_trajectories.pop(id))
                buffer.append(last_experience)

            # For all agents with a decision step:
            for id in decision_steps:
                # If the agent requesting a decision has a "last observation",
                # create an experience from it and the decision step
                if id in agent_last_obs:
                    experience = experience_from(id, decision_steps, done=False)
                    agent_cumulative_reward[id] += decision_steps[id].reward
                    agent_trajectories[id].append(experience)

                # Store the observation as the new "last observation"
                agent_last_obs[id] = decision_steps[id].obs[0]

            # Generate an action for all the agents that requested a decision
            # Compute the values for each action given the observation
            actions_values = policy(decision_steps.obs[0])  # TODO implement policies

            # NOTE we could add some noise to the values here (to make it less
            # deterministic)

            # Pick the best action using argmax
            actions = np.argmax(actions_values, axis=1)
            actions.resize((len(decision_steps), 1))

            for agent_index, agent_id in enumerate(decision_steps.agent_id):
                agent_last_action[agent_id] = actions[agent_index]

            # Set the actions in the environment and perform a step in the simulation
            env.set_actions(behavior_name, actions)
            env.step()

        return buffer, np.mean(cumulative_rewards)
